package com.coorchice.supertextview;

import com.coorchice.library.SuperTextView;

import com.coorchice.supertextview.Adjuster.MoveEffectAdjuster;
import com.coorchice.supertextview.Adjuster.OpportunityDemoAdjuster;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class MainAbility extends Ability {
    private SuperTextView stv_24;
    private SuperTextView stv_adjust;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        stv_24 = (SuperTextView) findComponentById(ResourceTable.Id_stv_28);
        stv_adjust = (SuperTextView) findComponentById(ResourceTable.Id_stv_adjust);
        stv_24.setUrlImage("https://img2.baidu.com/it/u=3355464299,584008140&fm=26&fmt=auto&gp=0.jpg");
        stv_adjust.addAdjuster(new MoveEffectAdjuster().setOpportunity(SuperTextView.Adjuster.Opportunity.BEFORE_DRAWABLE))
                .setAutoAdjust(true)
                .startAnim();
        SuperTextView superTextView= (SuperTextView) findComponentById(ResourceTable.Id_stv_119);
        OpportunityDemoAdjuster opportunityDemoAdjuster1 = new OpportunityDemoAdjuster();
        opportunityDemoAdjuster1.setOpportunity(SuperTextView.Adjuster.Opportunity.BEFORE_DRAWABLE);
        superTextView.addAdjuster(opportunityDemoAdjuster1);
        superTextView.setAutoAdjust(true);
    }
}
