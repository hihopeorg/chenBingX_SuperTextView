package com.coorchice.supertextview.slice;

import com.coorchice.library.SuperTextView;
import com.coorchice.supertextview.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.element.PixelMapElement;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;

import java.io.IOException;

public class ApiAbilitySlice extends AbilitySlice {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_api);
        SuperTextView tv40= (SuperTextView) findComponentById(ResourceTable.Id_stv_40);
        SuperTextView tv41= (SuperTextView) findComponentById(ResourceTable.Id_stv_41);
        SuperTextView tv42= (SuperTextView) findComponentById(ResourceTable.Id_stv_42);
        SuperTextView tv43= (SuperTextView) findComponentById(ResourceTable.Id_stv_43);
        SuperTextView tv44= (SuperTextView) findComponentById(ResourceTable.Id_stv_44);
        SuperTextView tv45= (SuperTextView) findComponentById(ResourceTable.Id_stv_45);
        SuperTextView tv46= (SuperTextView) findComponentById(ResourceTable.Id_stv_46);
        SuperTextView tv47= (SuperTextView) findComponentById(ResourceTable.Id_stv_47);
        SuperTextView tv48= (SuperTextView) findComponentById(ResourceTable.Id_stv_48);

        tv40.setCorner(20);
        tv40.setSolid(getColor(ResourceTable.Color_color_1));

        tv41.setCorner(130);
        tv41.setSolid(getColor(ResourceTable.Color_color_2));

        tv42.setSolid(getColor(ResourceTable.Color_color_3));
        tv42.setStrokeWidth(15);
        tv42.setStrokeColor(getColor(ResourceTable.Color_stroke_color));

        tv43.setCorner(120);
        tv43.setSolid(getColor(ResourceTable.Color_stroke_color));

        tv44.setCorner(45);
        tv44.setSolid(getColor(ResourceTable.Color_color_2));
        tv44.setLeftTopCornerEnable(true);

        tv45.setCorner(45);
        tv45.setSolid(getColor(ResourceTable.Color_pressed_solid_color));
        tv45.setRightTopCornerEnable(true);
        tv46.setCorner(45);
        tv46.setSolid(getColor(ResourceTable.Color_color_3));
        tv46.setLeftBottomCornerEnable(true);

        tv47.setCorner(45);
        tv47.setSolid(getColor(ResourceTable.Color_color_1));
        tv47.setRightBottomCornerEnable(true);

        PixelMapElement defaultPixelMapElement= null;
        try {
            Resource resource = getResourceManager().getResource(ResourceTable.Media_emoji_smoke);
            defaultPixelMapElement = new PixelMapElement(resource);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        }
        tv48.setCorner(48).setSolid(getColor(ResourceTable.Color_color_1)).setShowState(true).setSongDrawable(defaultPixelMapElement);

    }
}
