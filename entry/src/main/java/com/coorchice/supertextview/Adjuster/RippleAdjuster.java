/*
 * Copyright (C) 2017 CoorChice <icechen_@outlook.com>
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 * <p>
 * Last modified 17-4-17 下午5:44
 */

package com.coorchice.supertextview.Adjuster;

import com.coorchice.library.SuperTextView;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;


/**
 * Project Name:SuperTextView
 * Author:CoorChice
 * Date:2017/4/17
 * Notes:
 */

public class RippleAdjuster extends SuperTextView.Adjuster{
  private static final float DEFAULT_RADIUS = 50;

  private BlendMode xfermode = BlendMode.SRC_IN;
  private int rippleColor = Color.getIntColor("#ce938d");
  private float x = -1;
  private float y = -1;
  private Paint paint;
  private float density;
  private float radius = DEFAULT_RADIUS;
  private RectFloat rectF = new RectFloat();
  private float velocity = 2f;
  private Path solidPath;
  private RectFloat solidRectF;

  private PixelMap src;
  private Canvas srcCanvas;
  private PixelMap dst;
  private Canvas dstCanvas;


  public RippleAdjuster(int rippleColor) {
    this.rippleColor = rippleColor;
    initPaint();
  }

  private void initPaint() {
    paint = new Paint();
    paint.setAntiAlias(true);
    paint.setColor(new Color(rippleColor));
  }


  @Override
  protected void adjust(SuperTextView v, Canvas canvas) {
    int width = v.getWidth();
    int height = v.getHeight();
    if (srcCanvas == null){
      PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
      options.size = new Size(width,height);
      options.pixelFormat = PixelFormat.ARGB_8888;

      src = PixelMap.create(options);
      srcCanvas = new Canvas(new Texture(src));
    }
    if (dstCanvas == null){
      PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
      options.size = new Size(width,height);
      options.pixelFormat = PixelFormat.ARGB_8888;
      dst = PixelMap.create(options);
      dstCanvas = new Canvas(new Texture(dst));
    }
    if (density == 0) {
      density = 3.0f;
    }
    if (x == -1) {
      x = width / 2;
    }
    if (y == -1) {
      x = height / 2;

    }
    if (radius < ((float) width) * 1.1) {
      radius = (radius + velocity);
    } else {
      v.stopAnim();
    }
    rectF.clear();
    rectF.modify(0, 0, width, height);

    if (solidPath == null) {
      solidPath = new Path();
    } else {
      solidPath.reset();
    }
    if (solidRectF == null) {
      solidRectF = new RectFloat();
    } else {
      solidRectF.clear();
    }
    float strokeWidth = v.getStrokeWidth();
    solidRectF.modify(strokeWidth, strokeWidth, v.getWidth() - strokeWidth,
      v.getHeight() - strokeWidth);
    solidPath.addRoundRect(solidRectF, v.getCorners(), Path.Direction.CLOCK_WISE);

    paint.setColor(Color.WHITE);
    paint.setStyle(Paint.Style.FILL_STYLE);
    srcCanvas.drawColor(Color.TRANSPARENT.getValue(), Canvas.PorterDuffMode.CLEAR);
    srcCanvas.drawPath(solidPath, paint);

    paint.setColor(new Color(rippleColor));
    dstCanvas.drawColor(Color.TRANSPARENT.getValue(), Canvas.PorterDuffMode.CLEAR);
    dstCanvas.drawCircle(x, y, radius * density, paint);
    // 创建一个图层，在图层上演示图形混合后的效果
    int sc = 0;
    sc = canvas.saveLayer(new RectFloat(0, 0, width, height), new Paint());

    canvas.drawPixelMapHolder(new PixelMapHolder(src), 0, 0, paint);
    paint.setBlendMode(xfermode);
    canvas.drawPixelMapHolder(new PixelMapHolder(dst), 0, 0, paint);

    paint.setBlendMode(null);
    canvas.restoreToCount(sc);
  }

  @Override
  public boolean onTouch(SuperTextView v, TouchEvent event) {
    MmiPoint point = event.getPointerPosition(0);
    int action = event.getAction();
    switch (action) {
      case TouchEvent.PRIMARY_POINT_DOWN:
        x = point.getX();
        y = point.getY();
        radius = DEFAULT_RADIUS;
        v.setAutoAdjust(true);
        v.startAnim();
        break;
      case TouchEvent.PRIMARY_POINT_UP:
      case TouchEvent.CANCEL:
        v.stopAnim();
        v.setAutoAdjust(false);
        break;
    }
    return true;
  }
}
