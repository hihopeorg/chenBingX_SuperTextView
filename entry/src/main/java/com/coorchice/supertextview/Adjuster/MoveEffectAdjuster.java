/*
 * Copyright (C) 2017 CoorChice <icechen_@outlook.com>
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 * <p>
 * Last modified 17-4-17 下午3:23
 */

package com.coorchice.supertextview.Adjuster;

import com.coorchice.library.ResourceTable;
import com.coorchice.library.SuperTextView;

import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

/**
 * Project Name:SuperTextView
 * Author:CoorChice
 * Date:2017/4/17
 * Notes:
 */

public class MoveEffectAdjuster extends SuperTextView.Adjuster {

    private BlendMode xfermode = BlendMode.SRC_IN;
    private float totalWidth = 50f;
    private float startLocation = -99999f;
    private Paint paint = new Paint();
    private Path firstLinePath = new Path();
    private Path secondLinePath = new Path();
    private RectFloat rectF = new RectFloat();
    private float density;

    private PixelMap src;
    private Canvas srcCanvas;
    private PixelMap dst;
    private Canvas dstCanvas;
    private PixelMap render;
    private Canvas renderCanvas;


    @Override
    public void adjust(SuperTextView v, Canvas canvas) {
        int width = v.getWidth();
        int height = v.getHeight();
        if (density == 0) {
            density = 3.0f;
        }
        if (startLocation == -99999f) {
            startLocation = (float) (Math.random() * width);
        }
        if (startLocation < -(totalWidth * density + height * Math.tan(60))) {
            startLocation = width;
        }
        if (renderCanvas == null) {
            PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
            options.size = new Size(width, height);
            options.pixelFormat = PixelFormat.ARGB_8888;
            render = PixelMap.create(options);
            renderCanvas = new Canvas(new Texture(render));
        }

        if (dstCanvas == null) {
            PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
            options.size = new Size(width, height);
            options.pixelFormat = PixelFormat.ARGB_8888;
            dst = PixelMap.create(options);
            dstCanvas = new Canvas(new Texture(dst));
        }
        int firstLineWidth = (int) (totalWidth * density / 5 * 3);
        double velocity = 1.5 * density;
        startLocation = ((float) (startLocation - velocity));
        firstLinePath.reset();
        firstLinePath.moveTo(startLocation, height);
        firstLinePath.lineTo(startLocation + firstLineWidth, height);
        firstLinePath.lineTo((float) (startLocation + firstLineWidth + height * Math.tan(60)), 0);
        firstLinePath.lineTo((float) (startLocation + height * Math.tan(60)), 0);
        firstLinePath.close();
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.FILL_STYLE);
        int secondLineWidth = (int) (totalWidth * density / 5);
        secondLinePath.reset();
        secondLinePath.moveTo(startLocation + secondLineWidth * 4, height);
        secondLinePath.lineTo(startLocation + secondLineWidth * 4 + secondLineWidth, height);
        secondLinePath.lineTo(
                (float) (startLocation + secondLineWidth * 4 + secondLineWidth + height * Math.tan(60)),
                0);
        secondLinePath.lineTo((float) (startLocation + secondLineWidth * 4 + height * Math.tan(60)),
                0);
        secondLinePath.close();

        firstLinePath.addPath(secondLinePath);
        rectF.clear();
        rectF.modify(0, 0, width, height);

        if (srcCanvas == null) {
            PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
            options.size = new Size(width, height);
            options.pixelFormat = PixelFormat.ARGB_8888;
            src = PixelMap.create(options);
            srcCanvas = new Canvas(new Texture(src));

            paint.setColor(Color.WHITE);
            srcCanvas.drawColor(Color.TRANSPARENT.getValue(), Canvas.PorterDuffMode.CLEAR);
            srcCanvas.drawRoundRect(rectF, height / 2, height / 2, paint);
        }

        paint.setColor(new Color(Color.getIntColor("#bf000000")));
        dstCanvas.drawColor(Color.TRANSPARENT.getValue(), Canvas.PorterDuffMode.CLEAR);
        dstCanvas.drawPath(firstLinePath, paint);

        renderCanvas.drawPixelMapHolder(new PixelMapHolder(src), 0, 0, paint);
        paint.setBlendMode(xfermode);
        renderCanvas.drawPixelMapHolder(new PixelMapHolder(dst), 0, 0, paint);
        paint.setBlendMode(null);
        canvas.drawPixelMapHolder(new PixelMapHolder(render), 0, 0, paint);
    }
}
