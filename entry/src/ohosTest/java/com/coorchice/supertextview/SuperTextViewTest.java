/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.coorchice.supertextview;

import com.coorchice.library.SuperTextView;
import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * 功能性测试
 */
public class SuperTextViewTest extends TestCase {
    private Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    private MainAbility ability;
    private ShapeElement element;

    @Before
    public void setUp() throws Exception {
        ability = EventHelper.startAbility(MainAbility.class);
        EventHelper.waitForActive(ability, 5);
    }

    @After
    public void tearDown() throws Exception {
     EventHelper.clearAbilities();
    }

    @Test
    public void testAbility() {
        assertNotNull("testAbility failed, can not start ability", ability);

    }

    @Test
    public void testStrokeWidth(){
        SuperTextView superTextView = (SuperTextView) ability.findComponentById(ResourceTable.Id_stv_1);
        assertEquals("testCornerRadius",15.0f,superTextView.getStrokeWidth());
    }

    @Test
    public void testCornerRadius (){
        SuperTextView superTextView = (SuperTextView) ability.findComponentById(ResourceTable.Id_stv_0);
        assertEquals("testCornerRadius",30.0f,superTextView.getCorner());
    }

    @Test
    public void testLeftTopCorner(){
        SuperTextView superTextView = (SuperTextView) ability.findComponentById(ResourceTable.Id_stv_6);
        assertTrue("testLeftTopCorner", superTextView.isLeftTopCornerEnable());
    }
    @Test
    public void testRightTopCorner(){
        SuperTextView superTextView = (SuperTextView) ability.findComponentById(ResourceTable.Id_stv_7);
        assertTrue("testRightTopCorner", superTextView.isRightTopCornerEnable());
    }
    @Test
    public void testLeftBottomCorner(){
        SuperTextView superTextView = (SuperTextView) ability.findComponentById(ResourceTable.Id_stv_8);
        assertTrue("testLeftBottomCorner", superTextView.isLeftBottomCornerEnable());
    }
    @Test
    public void testRightBottomCorner(){
        SuperTextView superTextView = (SuperTextView) ability.findComponentById(ResourceTable.Id_stv_9);
        assertTrue("testRightBottomCorner", superTextView.isRightBottomCornerEnable());
    }
    @Test
    public void  testStrokeColor(){
        SuperTextView superTextView = (SuperTextView) ability.findComponentById(ResourceTable.Id_stv_5);
        assertEquals("testCornerRadius", Color.getIntColor("#5166ED"),superTextView.getStrokeColor());
    }
    @Test
    public void  testDrawable(){
        SuperTextView superTextView = (SuperTextView) ability.findComponentById(ResourceTable.Id_stv_12);
        assertNotNull("testDrawable",superTextView.getDrawable());
    }
    @Test
    public void  testImageUri(){
        SuperTextView superTextView = (SuperTextView) ability.findComponentById(ResourceTable.Id_stv_28);
        superTextView.setUrlImage("https://img2.baidu.com/it/u=3355464299,584008140&fm=26&fmt=auto&gp=0.jpg");
        //加载网络图片  耗时操作
                try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        PixelMapElement  element = (PixelMapElement) superTextView.getBackgroundElement();
        assertNotNull("testImageUri",element);


    }
    @Test
    public void testChangeColor(){
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        SuperTextView superTextView =(SuperTextView) ability.findComponentById(ResourceTable.Id_stv_29);
        EventHelper.inputSwipe(ability, 540 ,1800 ,540 ,200,1000);
        ShapeElement mSolidShapeElement = (ShapeElement) superTextView.getBackgroundElement();
        RgbColor rgbColor = mSolidShapeElement.getRgbColors()[0];
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        EventHelper.inputSwipe(ability,superTextView,540,20,540,20,1000);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        RgbColor rgbColor1 = mSolidShapeElement.getRgbColors()[0];
        Assert.assertTrue("点击后背景色无变化",rgbColor.asArgbInt()!=rgbColor1.asArgbInt());
    }
}