# SuperTextView

**本项目是基于开源项目SuperTextView进行ohos的移植和开发的，可以通过项目标签以及github地址（https://github.com/chenBingX/SuperTextView ）追踪到原项目版本**

#### 项目介绍

- 项目名称：SuperTextView
- 所属系列：ohos的第三方组件适配移植
- 功能：   
     1. 为 View 设置圆角  
     2. 支持单独控制每一个圆角  
     3. 为 View 添加边框  
     4. 支持Drawable 展示  
     5. 可准确控制 Drawable 大小及位置  
     6. 支持渐变色背景  
     7. 触摸变色  
     8. 展示图片，包括网络图片  
     9. 为图片设置圆角  
     10. 为图片增加边框  
     11. 修改 Drawable 的颜色  
     12. Drawable 的旋转角度
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/chenBingX/SuperTextView
- 原项目基线版本：v3.5.2.64,sha1:94ca8de365740f396ce2494301f85c08832272cb
- 编程语言：Java
- 外部库依赖：无

#### 效果展示
  <img src="screenshot/operation.gif"/>  

#### 安装教程

方法一：

1. 编译har包SuperTextView.har。
2. 启动 DevEco Studio，将编译的har包，导入工程目录“app->libs”下。

3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```groovy
repositories {
    flatDir { dirs 'libs' }
}
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation(name: 'SuperTextView', ext: 'har')

}
```

方法二：

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址

   ```
   repositories {
       maven {
           url 'http://106.15.92.248:8081/repository/Releases/' 
       }
   }
   ```

   

2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

   ```
   dependencies {
       implementation 'com.coorchice.ohos:supertextview:1.0.1'
   }
   ```

   

#### 使用说明

1.边框+圆角

```xml
            <com.coorchice.library.SuperTextView
                ohos:id="$+id:stv_2"
                ohos:height="80vp"
                ohos:width="80vp"
                ohos:left_margin="10vp"
                ohos:text="圆角边框"
                ohos:text_alignment="center"
                ohos:text_color="#F3FAF3F3"
                ohos:text_size="14fp"
                app:stv_corner="10vp"
                app:stv_solid="#4F94CD"
                app:stv_stroke_color="#5166ED"
                app:stv_stroke_width="5vp"/>
```
2.指定圆角位置（left_top/right_top/left_bottom/right_bottom）
```xml
         <com.coorchice.library.SuperTextView
                    ohos:id="$+id:stv_6"
                    ohos:height="50vp"
                    ohos:width="50vp"
                    ohos:left_margin="5vp"
                    ohos:text="左上圆角"
                    ohos:text_alignment="center"
                    ohos:text_color="#F3FAF3F3"
                    ohos:text_size="10fp"
                    app:stv_corner="15vp"
                    app:stv_left_top_corner="true"
                    app:stv_solid="#85219E"/>
```
3.状态图（图文混搭显示）
```xml
        <com.coorchice.library.SuperTextView
                ohos:id="$+id:stv_12"
                ohos:height="80vp"
                ohos:width="80vp"
                ohos:left_margin="10vp"
                ohos:text="状态图1"
                ohos:text_alignment="bottom|horizontal_center"
                ohos:text_color="#F3F8FBFB"
                ohos:text_size="14fp"
                app:stv_corner="10vp"
                app:stv_isShowState="true"
                app:stv_solid="#4CBDD2"
                app:stv_state_drawable="$media:emoji_smoke"/>
```
4.改变图片角度及颜色
```xml
        <com.coorchice.library.SuperTextView
                ohos:id="$+id:stv_22"
                ohos:height="50vp"
                ohos:width="50vp"
                ohos:left_margin="10vp"
                ohos:text_alignment="bottom|horizontal_center"
                ohos:text_color="#F30E0F0F"
                ohos:text_size="14fp"
                app:stv_isShowState="true"
                app:stv_solid="#A58FED"
                app:stv_state_drawable="$media:arrow"
                app:stv_state_drawable_rotate="180"/>
            <com.coorchice.library.SuperTextView
                ohos:id="$+id:stv_23"
                ohos:height="50vp"
                ohos:width="50vp"
                ohos:left_margin="10vp"
                ohos:text_alignment="bottom|horizontal_center"
                ohos:text_color="#F30E0F0F"
                ohos:text_size="14fp"
                app:stv_isShowState="true"
                app:stv_solid="#A58FED"
                app:stv_state_drawable="$media:arrow"
                app:stv_state_drawable_rotate="270"/>
```
5.支持网络图片
```java
stv_24.setUrlImage("https://img2.baidu.com/it/u=3355464299,584008140&fm=26&fmt=auto&gp=0.jpg");
```
6.背景渐变
```xml
        <com.coorchice.library.SuperTextView
            ohos:id="$+id:stv_53"
            ohos:height="50vp"
            ohos:width="match_parent"
            ohos:top_margin="10vp"
            ohos:left_margin="10vp"
            ohos:text_alignment="center"
            ohos:text="背景渐变"
            ohos:text_color="#F30E0F0F"
            ohos:text_size="14fp"
            app:stv_solid="#FF257FD0"
            app:stv_corner="10vp"
            app:stv_shaderEnable="true"
            app:stv_shaderEndColor="#FFDE36A0"
            app:stv_shaderMode="1"
            app:stv_shaderStartColor="#FF257FD0"/>
```
7.背景点击效果
```xml
        <com.coorchice.library.SuperTextView
            ohos:id="$+id:stv_29"
            ohos:height="50vp"
            ohos:width="match_parent"
            ohos:left_margin="10vp"
            ohos:text_alignment="center"
            ohos:text="点我变色"
            ohos:text_color="#F30E0F0F"
            ohos:text_size="14fp"
            app:stv_solid="#FF257FD0"
            app:stv_pressBgColor="#FF12CE31"
            app:stv_pressTextColor="#FFE50F0F"/>
```
8.图片边框及圆角设置
```xml
           <com.coorchice.library.SuperTextView
                ohos:id="$+id:stv_36"
                ohos:height="100vp"
                ohos:width="100vp"
                ohos:left_margin="10vp"
                app:stv_drawableAsBackground="true"
                app:stv_state_drawable="$media:avatar1"
                app:stv_stroke_color="#FFB02020"
                app:stv_stroke_width="5vp" />
```
#### 版本迭代


- v1.0.1


#### 版权和许可信息
```
Copyright 2017 CoorChice

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

