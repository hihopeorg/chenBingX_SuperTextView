package com.coorchice.library.constant;

public interface TextConstant {
    String CORNER = "stv_corner";
    String LEFT_TOP_CORNER = "stv_left_top_corner";
    String RIGHT_TOP_CORNER = "stv_right_top_corner";
    String LEFT_BOTTOM_CORNER = "stv_left_bottom_corner";
    String RIGHT_BOTTOM_CORNER = "stv_right_bottom_corner";

    String SOLID_COLOR = "stv_solid";
    String STROKE_WIDTH = "stv_stroke_width";
    String STROKE_COLOR = "stv_stroke_color";
    String STATE_DRAWABLE = "stv_state_drawable";
    String STATE_DRAWABLE_WIDTH = "stv_state_drawable_width";
    String STATE_DRAWABLE_HEIGHT = "stv_state_drawable_height";
    String STATE_DRAWABLE_PADDING_LEFT = "stv_state_drawable_padding_left";
    String STATE_DRAWABLE_PADDING_TOP = "stv_state_drawable_padding_top";
    String STATE_DRAWABLE_TINT = "stv_state_drawable_tint";
    String STATE_DRAWABLE_ROTATE = "stv_state_drawable_rotate";
    String STATE_DRAWABLE2 = "stv_state_drawable2";
    String STATE_DRAWABLE2_WIDTH = "stv_state_drawable2_width";
    String STATE_DRAWABLE2_HEIGHT = "stv_state_drawable2_height";
    String STATE_DRAWABLE2_PADDING_LEFT = "stv_state_drawable2_padding_left";
    String STATE_DRAWABLE2_PADDING_TOP = "stv_state_drawable2_padding_top";
    String STATE_DRAWABLE2_TINT = "stv_state_drawable2_tint";
    String STATE_DRAWABLE2_ROTATE = "stv_state_drawable2_rotate";
    String IS_SHOW_STATE = "stv_isShowState";
    String DRAWABLE_AS_BACKGROUND = "stv_drawableAsBackground";
    String SCALE_TYPE = "stv_scaleType";

    String IS_SHOW_STATE2 = "stv_isShowState2";
    String TEXT_STROKE = "stv_text_stroke";
    String TEXT_STROKE_COLOR = "stv_text_stroke_color";
    String TEXT_STROKE_WIDTH = "stv_text_stroke_width";
    String TEXT_FILL_COLOR = "stv_text_fill_color";
    String AUTO_ADJUST = "stv_autoAdjust";
    String STATE_DRAWABLE_LAYER = "stv_state_drawable_layer";
    String STATE_DRAWABLE_LAYER2 = "stv_state_drawable2_layer";
    String STATE_DRAWABLE_MODE = "stv_state_drawable_mode";
    String STATE_DRAWABLE2_MODE = "stv_state_drawable2_mode";

    String SHADER_START_COLOR = "stv_shaderStartColor";
    String SHADER_END_COLOR = "stv_shaderEndColor";
    String SHADER_MODE = "stv_shaderMode";

    String SHADER_ENABLE = "stv_shaderEnable";
    String TEXT_SHADER_START_COLOR = "stv_textShaderStartColor";
    String TEXT_SHADER_END_COLOR = "stv_textShaderEndColor";
    String TEXT_SHADER_MODE = "stv_textShaderMode";
    String TEXT_SHADER_ENABLE = "stv_textShaderEnable";
    String PRESS_BG_COLOR = "stv_pressBgColor";
    String PRESS_TEXT_COLOR = "stv_pressTextColor";

}
