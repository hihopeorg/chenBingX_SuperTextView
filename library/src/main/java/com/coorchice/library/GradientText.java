package com.coorchice.library;

import com.coorchice.library.utils.track.Event;
import com.coorchice.library.utils.track.TimeEvent;
import com.coorchice.library.utils.track.Tracker;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

import java.sql.Ref;

public class GradientText extends Text  implements Component.DrawTask {
    private static HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x0000101, "GradientText");
    private  Paint mPaint;
    private Path strokeWidthPath;
    private Path solidPath;
    private RectFloat strokeLineRectF;
    private RectFloat solidRectF;
    private PixelMapElement  pixelMapElement;
    private PixelMap pixelMap;
    private final Matrix mShaderMatrix = new Matrix();
    private RectFloat  rectFloat=new RectFloat();

    private int[] suitedSize;
    private int width;
    private int height;
    private SuperTextView.ScaleType backgroundScaleType = SuperTextView.ScaleType.CENTER;

    public GradientText(Context context) {
        super(context);
    }

    public GradientText(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(attrSet);
    }

    public GradientText(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }

    private void init(AttrSet attrSet){
        pixelMapElement = (PixelMapElement) attrSet.getAttr("stv_pixelmap").get().getElement();
        mPaint = new Paint();
        initPaint();
        addDrawTask(this::onDraw);
    }
    private void initPaint() {
        mPaint.reset();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setFilterBitmap(true);
    }
    @Override
    public void onDraw(Component component, Canvas canvas) {
        width = getWidth();
        height = getHeight();
        HiLog.info(label,"component width:%{public}d,height:%{public}d",width,height);
        pixelMap  = pixelMapElement.getPixelMap();
        HiLog.info(label,"pixelMapElement width:%{public}d",pixelMapElement.getWidth());
        Texture texture = new Texture(pixelMap);
        suitedSize = computeSuitedBitmapSize(pixelMapElement);
        HiLog.info(label,"texture width:%{public}d,height:%{public}d",texture.getWidth(),texture.getHeight());
        PixelMapHolder pixelMapHolder = new PixelMapHolder(pixelMap);
        // 绘制图片，使用之前计算的数据
            PixelMapShader bitmapShader = new PixelMapShader(pixelMapHolder, Shader.TileMode.CLAMP_TILEMODE, Shader.TileMode.CLAMP_TILEMODE);
            bitmapShader.setShaderMatrix(mShaderMatrix);
        mPaint.setShader(bitmapShader, Paint.ShaderType.PIXELMAP_SHADER);
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        HiLog.info(label, "Log_圆图绘制");
        rectFloat.modify(0,0,suitedSize[0],suitedSize[1]);

        canvas.drawRoundRect(rectFloat,0,0,mPaint);
//        pixelMapElement.drawToCanvas(canvas);
        mPaint.reset();
        mPaint.setStrokeWidth(20.0f);
        mPaint.setColor(Color.RED);
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        rectFloat.modify(0,0,suitedSize[0],suitedSize[1]);
        canvas.drawRoundRect(rectFloat,0,0,mPaint);

//         canvas.drawRect(solidRectF,mPaint);
    }
    private void drawDrawableBackground(Canvas canvas) {
    }

    private int[] computeSuitedBitmapSize(Element drawable) {
        int dWidth = drawable.getWidth();
        int dHeight = drawable.getWidth();
        if (!(dWidth > 0)) {
            dWidth = width;
        }
        if (!(dHeight > 0)) {
            dHeight = height;
        }
        int suitedWidth = width;
        int suitedHeight = height;
        if (suitedSize == null) {
            suitedSize = new int[4];
        }
        if (backgroundScaleType == SuperTextView.ScaleType.FIT_CENTER) {
            if ((float) dWidth / (float) width > (float) dHeight / (float) height) {
                suitedHeight = (int) ((float) suitedWidth / ((float) dWidth / (float) dHeight));
            } else {
                suitedWidth = (int) (((float) dWidth / (float) dHeight) * (float) suitedHeight);
            }
            suitedSize[0] = suitedWidth;
            suitedSize[1] = suitedHeight;
            suitedSize[2] = width / 2 - suitedSize[0] / 2;
            suitedSize[3] = height / 2 - suitedSize[1] / 2;
        } else if (backgroundScaleType == SuperTextView.ScaleType.FIT_XY) {
            suitedSize[0] = suitedWidth;
            suitedSize[1] = suitedHeight;
            suitedSize[2] = 0;
            suitedSize[3] = 0;
        } else {
            if ((float) dWidth / (float) width > (float) dHeight / (float) height) {
                suitedWidth = (int) (((float) dWidth / (float) dHeight) * (float) suitedHeight);
            } else {
                suitedHeight = (int) ((float) suitedWidth / ((float) dWidth / (float) dHeight));
            }
            suitedSize[0] = suitedWidth;
            suitedSize[1] = suitedHeight;
            suitedSize[2] = -(suitedSize[0] / 2 - width / 2);
            suitedSize[3] = -(suitedSize[1] / 2 - height / 2);
        }
        return suitedSize;
    }
}
