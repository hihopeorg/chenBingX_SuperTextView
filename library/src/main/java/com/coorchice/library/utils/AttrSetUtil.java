package com.coorchice.library.utils;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextTool;

public class AttrSetUtil {

    public static Color getColor(AttrSet attrSet, String attrKey,int colorValue) {
        if (attrSet.getAttr(attrKey).isPresent()) {
            return attrSet.getAttr(attrKey).get().getColorValue();
        }
        return new Color(colorValue);
    }

    public static Element getElement(AttrSet attrSet, String attrKey) {
        if (attrSet.getAttr(attrKey).isPresent()) {
            return attrSet.getAttr(attrKey).get().getElement();
        }
        return null;
    }

    public static int  getInt(AttrSet attrSet,String attrKey,int defaultValue){
        if (attrSet.getAttr(attrKey).isPresent()) {
            return attrSet.getAttr(attrKey).get().getIntegerValue();
        }
        return defaultValue;
    }

    public static float  getFloat(AttrSet attrSet,String attrKey,float defaultValue){
        if (attrSet.getAttr(attrKey).isPresent()) {
            return attrSet.getAttr(attrKey).get().getFloatValue();
        }
        return defaultValue;
    }

    public static boolean  getBoolean(AttrSet attrSet,String attrKey,boolean defaultValue){
        if (attrSet.getAttr(attrKey).isPresent()) {
            return attrSet.getAttr(attrKey).get().getBoolValue();
        }
        return defaultValue;
    }
    public static String  getString(AttrSet attrSet,String attrKey){
        if (attrSet.getAttr(attrKey).isPresent()) {
            return attrSet.getAttr(attrKey).get().getStringValue();
        }
        return "";
    }

    public static float  getDimension(AttrSet attrSet,String attrKey,float defaultValue){
        if (attrSet.getAttr(attrKey).isPresent()) {
            return attrSet.getAttr(attrKey).get().getDimensionValue();
        }
        return defaultValue;
    }
}
