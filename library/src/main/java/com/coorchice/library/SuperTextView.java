

/*
 * Copyright (C) 2017 CoorChice <icechen_@outlook.com>
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 * <p>
 * Last modified 17-4-20 下午5:32
 */

package com.coorchice.library;


import com.coorchice.library.constant.TextConstant;
import com.coorchice.library.utils.AttrSetUtil;
import com.coorchice.library.utils.track.Event;
import com.coorchice.library.utils.track.TimeEvent;
import com.coorchice.library.utils.track.Tracker;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.element.*;
import ohos.agp.render.*;
import ohos.agp.utils.*;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import java.util.ArrayList;
import java.util.List;

public class SuperTextView extends Text implements Component.DrawTask, Component.BindStateChangedListener,
        Component.TouchEventListener {

    private static HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x0000100, "SuperTextView");
    /**
     * 标识不进行颜色的更改
     */
    public static final int NO_COLOR = -99;
    /**
     * 标识不进行旋转
     */
    public static final float NO_ROTATE = -1000;
    // Some Property Default Value
    private static final int DEFAULT_CORNER = 0;
    private static final int DEFAULT_SOLID = 0;
    private static final float DEFAULT_STROKE_WIDTH = 0f;
    private static final int DEFAULT_STROKE_COLOR = 0xFF000000;
    private static final int DEFAULT_STATE_DRAWABLE_MODE = DrawableMode.CENTER.code;
    private static final int DEFAULT_STATE_DRAWABLE_LAYER = DrawableLayer.BEFORE_TEXT.code;
    private static final int DEFAULT_TEXT_STROKE_COLOR = 0xFF000000;
    private static final int DEFAULT_TEXT_FILL_COLOR = 0xFF000000;
    private static final float DEFAULT_TEXT_STROKE_WIDTH = 0f;
    private static final int ALLOW_CUSTOM_ADJUSTER_SIZE = 3;
    private int SYSTEM_ADJUSTER_SIZE = 0;

    private float corner;
    private boolean leftTopCornerEnable;
    private boolean rightTopCornerEnable;
    private boolean leftBottomCornerEnable;
    private boolean rightBottomCornerEnable;
    private int solid;
    private float strokeWidth;
    private int strokeColor;
    private int stateDrawableLayer = DrawTask.BETWEEN_BACKGROUND_AND_CONTENT;
    private int stateDrawable2Layer = DrawTask.BETWEEN_BACKGROUND_AND_CONTENT;
    private String stateDrawableMode;
    private String stateDrawable2Mode;
    private boolean isShowState;
    private boolean isShowState2;

    private Paint paint;
    private int width;
    private int height;
    private Element songDrawable;
    private Element drawable2;
    private boolean autoAdjust;
    // private Adjuster adjuster;
    private Adjuster pressAdjuster;
    private boolean textStroke;
    private int textStrokeColor;
    private int textFillColor;
    private float textStrokeWidth;
    private boolean runnable = false;
    private boolean needRun = false;
    private Thread animThread;
    private Path strokeWidthPath;
    private Path solidPath;
    private RectFloat strokeLineRectF;
    private RectFloat solidRectF;
    private float leftTopCorner[] = new float[2];
    private float rightTopCorner[] = new float[2];
    private float leftBottomCorner[] = new float[2];
    private float rightBottomCorner[] = new float[2];
    private float corners[] = new float[8];
    private float[] drawableBounds = new float[4];
    private float drawableWidth;
    private float drawableHeight;
    private float drawablePaddingLeft;
    private float drawablePaddingTop;
    private float[] drawable2Bounds = new float[4];
    private float drawable2Width;
    private float drawable2Height;
    private float drawable2PaddingLeft;
    private float drawable2PaddingTop;
    private boolean cacheRunnableState, cacheDrawablePlaying, cacheDrawable2Playing;
    private boolean cacheNeedRunState;
    private int frameRate = 60;
    private Runnable invalidate;
    private int shaderStartColor;
    private int shaderEndColor;
    private String shaderMode;
    private LinearShader solidShader;
    private boolean shaderEnable;
    private int textShaderStartColor;
    private int textShaderEndColor;
    private ShaderMode textShaderMode;
    private boolean textShaderEnable;
    private LinearShader textShader;
    private int pressBgColor = NO_COLOR;
    private int pressTextColor = NO_COLOR;
    private boolean drawableAsBackground;
    private Shader drawableBackgroundShader;

    private List<Adjuster> adjusterList = new ArrayList<>();
    private List<Adjuster> touchAdjusters = new ArrayList<>();
    private Runnable handleAnim;
    private boolean superTouchEvent;
    private boolean drawable1Clicked = false;
    private boolean drawable2Clicked = false;
    private String curImageUrl;

    private int drawableTint = NO_COLOR;
    private float drawableRotate = NO_ROTATE;
    private int drawable2Tint = NO_COLOR;
    private float drawable2Rotate = NO_ROTATE;

    private OnDrawableClickedListener onDrawableClickedListener;

    private int[] suitedSize;
    private Canvas drawableBgCanvas, tempDrawableBgCanvas, drawable1Canvas, drawable2Canvas;
    private PixelMap drawableBgCanvasBitmap, tempDrawableBgCanvasBitmap, drawable1CanvasBitmap, drawable2CanvasBitmap;
    private ScaleType backgroundScaleType = ScaleType.CENTER;
    private Rect orgBounds;
    private Tracker tracker;

    private ShapeElement mSolidShapeElement;

    private int mNormalColor;

    private boolean mIsPress;

    /**
     * 简单的构造函数
     *
     * @param context View运行的Context环境
     */
    public SuperTextView(Context context) {
        super(context);
        init(null);
    }

    /**
     * inflate Xml布局文件时会被调用
     *
     * @param context View运行的Context环境
     * @param attrs   View在xml布局文件中配置的属性集合对象
     */
    public SuperTextView(Context context, AttrSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    /**
     * 略
     *
     * @param context
     * @param attrs
     * @param defStyleAttr
     */
    public SuperTextView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }


    private void init(AttrSet attrs) {
        initAttrs(attrs);
        paint = new Paint();
        mSolidShapeElement = new ShapeElement();
        initPaint();
        addDrawTask(this::onDraw, stateDrawableLayer);
        setBindStateChangedListener(this);
        setTouchEventListener(this::onTouchEvent);
    }

    private void initAttrs(AttrSet attrs) {
        if (attrs != null) {
            corner = AttrSetUtil.getDimension(attrs, TextConstant.CORNER, DEFAULT_CORNER);
            leftTopCornerEnable = AttrSetUtil.getBoolean(attrs, TextConstant.LEFT_TOP_CORNER, false);
            rightTopCornerEnable = AttrSetUtil.getBoolean(attrs, TextConstant.RIGHT_TOP_CORNER, false);
            leftBottomCornerEnable = AttrSetUtil.getBoolean(attrs, TextConstant.LEFT_BOTTOM_CORNER, false);
            rightBottomCornerEnable = AttrSetUtil.getBoolean(attrs, TextConstant.RIGHT_BOTTOM_CORNER, false);
            solid = AttrSetUtil.getColor(attrs, TextConstant.SOLID_COLOR, DEFAULT_SOLID).getValue();
            strokeWidth = AttrSetUtil.getDimension(attrs, TextConstant.STROKE_WIDTH, DEFAULT_STROKE_WIDTH);
            strokeColor = AttrSetUtil.getColor(attrs, TextConstant.STROKE_COLOR, DEFAULT_STROKE_COLOR).getValue();
            songDrawable = AttrSetUtil.getElement(attrs, TextConstant.STATE_DRAWABLE);

            drawableWidth = AttrSetUtil.getDimension(attrs, TextConstant.STATE_DRAWABLE_WIDTH, 0);
            drawableHeight = AttrSetUtil.getDimension(attrs, TextConstant.STATE_DRAWABLE_HEIGHT, 0);
            drawablePaddingLeft = AttrSetUtil.getDimension(attrs, TextConstant.STATE_DRAWABLE_PADDING_LEFT, 0);
            drawablePaddingTop = AttrSetUtil.getDimension(attrs, TextConstant.STATE_DRAWABLE_PADDING_TOP, 0);
            drawableTint = AttrSetUtil.getColor(attrs, TextConstant.STATE_DRAWABLE_TINT, NO_COLOR).getValue();
            drawableRotate = AttrSetUtil.getFloat(attrs, TextConstant.STATE_DRAWABLE_ROTATE, NO_ROTATE);
            drawable2 = AttrSetUtil.getElement(attrs, TextConstant.STATE_DRAWABLE2);

            drawable2Width = AttrSetUtil.getDimension(attrs, TextConstant.STATE_DRAWABLE2_WIDTH, 0);
            drawable2Height = AttrSetUtil.getDimension(attrs, TextConstant.STATE_DRAWABLE2_HEIGHT, 0);
            drawable2PaddingLeft = AttrSetUtil.getDimension(attrs, TextConstant.STATE_DRAWABLE2_PADDING_LEFT, 0);
            drawable2PaddingTop = AttrSetUtil.getDimension(attrs, TextConstant.STATE_DRAWABLE2_PADDING_TOP, 0);
            drawable2Tint = AttrSetUtil.getColor(attrs, TextConstant.STATE_DRAWABLE2_TINT, NO_COLOR).getValue();
            drawable2Rotate = AttrSetUtil.getFloat(attrs, TextConstant.STATE_DRAWABLE2_ROTATE, NO_ROTATE);

            isShowState = AttrSetUtil.getBoolean(attrs, TextConstant.IS_SHOW_STATE, false);
            isShowState2 = AttrSetUtil.getBoolean(attrs, TextConstant.IS_SHOW_STATE2, false);
            drawableAsBackground = AttrSetUtil.getBoolean(attrs, TextConstant.DRAWABLE_AS_BACKGROUND, false);
            backgroundScaleType = ScaleType.valueOf(AttrSetUtil.getInt(attrs, TextConstant.SCALE_TYPE, ScaleType.CENTER.code));
            stateDrawableLayer = AttrSetUtil.getInt(attrs, TextConstant.STATE_DRAWABLE_LAYER,
                    DrawTask.BETWEEN_BACKGROUND_AND_CONTENT);
            stateDrawable2Layer = AttrSetUtil.getInt(attrs, TextConstant.STATE_DRAWABLE_LAYER2,
                    DrawTask.BETWEEN_BACKGROUND_AND_CONTENT);
            stateDrawableMode = AttrSetUtil.getString(attrs, TextConstant.STATE_DRAWABLE_MODE);
            stateDrawable2Mode = AttrSetUtil.getString(attrs, TextConstant.STATE_DRAWABLE2_MODE);

            textStroke = AttrSetUtil.getBoolean(attrs, TextConstant.TEXT_STROKE, false);
            textStrokeColor = AttrSetUtil.getColor(attrs, TextConstant.TEXT_STROKE_COLOR,
                    DEFAULT_TEXT_STROKE_COLOR).getValue();
            textFillColor = AttrSetUtil.getColor(attrs, TextConstant.TEXT_FILL_COLOR,
                    DEFAULT_TEXT_FILL_COLOR).getValue();
            textStrokeWidth = AttrSetUtil.getDimension(attrs, TextConstant.TEXT_STROKE_WIDTH,
                    DEFAULT_TEXT_STROKE_WIDTH);
            autoAdjust = AttrSetUtil.getBoolean(attrs, TextConstant.AUTO_ADJUST, false);
            shaderStartColor =
                    AttrSetUtil.getColor(attrs, TextConstant.SHADER_START_COLOR, NO_COLOR).getValue();
            shaderEndColor =
                    AttrSetUtil.getColor(attrs, TextConstant.SHADER_END_COLOR, NO_COLOR).getValue();
            shaderMode = AttrSetUtil.getString(attrs, TextConstant.SHADER_MODE);
            shaderEnable = AttrSetUtil.getBoolean(attrs, TextConstant.SHADER_ENABLE, false);

            textShaderStartColor =
                    AttrSetUtil.getColor(attrs, TextConstant.TEXT_SHADER_START_COLOR, NO_COLOR).getValue();
            textShaderEndColor =
                    AttrSetUtil.getColor(attrs, TextConstant.TEXT_SHADER_END_COLOR, NO_COLOR).getValue();
            textShaderMode =
                    ShaderMode.valueOf(AttrSetUtil.getInt(attrs, TextConstant.TEXT_SHADER_MODE, ShaderMode.TOP_TO_BOTTOM.code));
            textShaderEnable = AttrSetUtil.getBoolean(attrs, TextConstant.TEXT_SHADER_ENABLE, false);

            pressBgColor = AttrSetUtil.getColor(attrs, TextConstant.PRESS_BG_COLOR, NO_COLOR).getValue();
            pressTextColor = AttrSetUtil.getColor(attrs, TextConstant.PRESS_TEXT_COLOR, NO_COLOR).getValue();

        }
        if (getTextColor() != null) {
            mNormalColor = getTextColor().getValue();
        } else {
            mNormalColor = Color.BLACK.getValue();
        }
    }


    private void initPaint() {
        paint.reset();
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setFilterBitmap(true);

    }

    @Override
    public void invalidate() {
        addDrawTask(this::onDraw, stateDrawableLayer);
        super.invalidate();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (getVisibility() != VISIBLE || !isBoundToWindow() || getWidth() <= 0 || getHeight() <= 0)
            return;
        long startDrawTime = System.currentTimeMillis();
        Tracker.notifyEvent(tracker, TimeEvent.create(Event.OnDrawStart, startDrawTime));
        width = getWidth();
        height = getHeight();
        boolean needScroll = getScrollValue(AXIS_X) != 0 || getScrollValue(AXIS_Y) != 0;
        if (needScroll) {
            canvas.translate(getScrollValue(AXIS_X), getScrollValue(AXIS_Y));
        }
        drawSolid();
        isNeedToAdjust(canvas, Adjuster.Opportunity.BEFORE_DRAWABLE);
        long startDrawDrawableTime = System.currentTimeMillis();
        if (drawableAsBackground || stateDrawableLayer == DrawTask.BETWEEN_BACKGROUND_AND_CONTENT) {
            drawStateDrawable(canvas);
        }
        if (stateDrawable2Layer == DrawTask.BETWEEN_BACKGROUND_AND_CONTENT) drawStateDrawable2(canvas);

        Tracker.notifyEvent(tracker, TimeEvent.create(Event.OnDrawDrawableEnd, System.currentTimeMillis() - startDrawDrawableTime));
//        isNeedToAdjust(canvas, Adjuster.Opportunity.BEFORE_DRAWABLE);
        if (needScroll) {
            canvas.translate(-getScrollValue(AXIS_X), -getScrollValue(AXIS_Y));
        }
        if (textStroke) {
            drawTextStroke(canvas);
        }
        if (textShaderEnable) {
            drawShaderText(canvas);
        }
        if (needScroll) {
            canvas.translate(getScrollValue(AXIS_Y), getScrollValue(AXIS_Y));
        }
        if (!drawableAsBackground && stateDrawableLayer == DrawTask.BETWEEN_BACKGROUND_AND_CONTENT)
            drawStateDrawable(canvas);
        if (stateDrawable2Layer == DrawTask.BETWEEN_BACKGROUND_AND_CONTENT) drawStateDrawable2(canvas);
//        isNeedToAdjust(canvas, Adjuster.Opportunity.BEFORE_DRAWABLE);
        if (needScroll) {
            canvas.translate(-getScrollValue(AXIS_Y), -getScrollValue(AXIS_Y));
        }
        Tracker.notifyEvent(tracker, TimeEvent.create(Event.OnDrawEnd, System.currentTimeMillis() - startDrawTime));
    }

    private void drawSolid() {
        HiLog.info(label, "pressed:%{public}b", mIsPress);
        if (mSolidShapeElement == null) {
            mSolidShapeElement = new ShapeElement();
        }
        getCorners(corner - strokeWidth / 2);

        mSolidShapeElement.setCornerRadiiArray(corners);
        mSolidShapeElement.setRgbColor((pressBgColor != NO_COLOR && mIsPress) ? RgbColor.fromArgbInt(pressBgColor) : RgbColor.fromArgbInt(solid));

        if (strokeColor != NO_COLOR && strokeWidth != 0) {
            mSolidShapeElement.setStroke((int) strokeWidth, RgbColor.fromArgbInt(new Color(strokeColor).getValue()));
        }
        if (shaderStartColor != NO_COLOR && shaderEndColor != NO_COLOR) {
            RgbColor[] colors = new RgbColor[2];
            colors[0] = RgbColor.fromArgbInt(new Color(shaderStartColor).getValue());
            colors[1] = RgbColor.fromArgbInt(new Color(shaderEndColor).getValue());
            mSolidShapeElement.setGradientOrientation(getGradientOrientation());
            mSolidShapeElement.setShape(ShapeElement.RECTANGLE);
            mSolidShapeElement.setRgbColors(colors);
        }
        setBackground(mSolidShapeElement);
    }

    private ShapeElement.Orientation getGradientOrientation() {
        if (TextTool.isNullOrEmpty(shaderMode)) {
            return ShapeElement.Orientation.TOP_TO_BOTTOM;
        }
        if ("bottomToTop".equals(shaderMode)) {
            return ShapeElement.Orientation.BOTTOM_TO_TOP;
        } else if ("leftToRight".equals(shaderMode)) {
            return ShapeElement.Orientation.LEFT_TO_RIGHT;
        } else if ("rightToLeft".equals(shaderMode)) {
            return ShapeElement.Orientation.RIGHT_TO_LEFT;
        } else {
            return ShapeElement.Orientation.TOP_TO_BOTTOM;
        }
    }

    private float[] getCorners(float corner) {
        leftTopCorner[0] = 0;
        leftTopCorner[1] = 0;
        rightTopCorner[0] = 0;
        rightTopCorner[1] = 0;
        leftBottomCorner[0] = 0;
        leftBottomCorner[1] = 0;
        rightBottomCorner[0] = 0;
        rightBottomCorner[1] = 0;
        if (this.leftTopCornerEnable || this.rightTopCornerEnable || this.leftBottomCornerEnable
                || this.rightBottomCornerEnable) {
            if (this.leftTopCornerEnable) {
                leftTopCorner[0] = corner;
                leftTopCorner[1] = corner;
            }
            if (this.rightTopCornerEnable) {
                rightTopCorner[0] = corner;
                rightTopCorner[1] = corner;
            }
            if (this.leftBottomCornerEnable) {
                leftBottomCorner[0] = corner;
                leftBottomCorner[1] = corner;
            }
            if (this.rightBottomCornerEnable) {
                rightBottomCorner[0] = corner;
                rightBottomCorner[1] = corner;
            }
        } else {
            leftTopCorner[0] = corner;
            leftTopCorner[1] = corner;
            rightTopCorner[0] = corner;
            rightTopCorner[1] = corner;
            leftBottomCorner[0] = corner;
            leftBottomCorner[1] = corner;
            rightBottomCorner[0] = corner;
            rightBottomCorner[1] = corner;
        }
        corners[0] = leftTopCorner[0];
        corners[1] = leftTopCorner[1];
        corners[2] = rightTopCorner[0];
        corners[3] = rightTopCorner[1];
        corners[4] = rightBottomCorner[0];
        corners[5] = rightBottomCorner[1];
        corners[6] = leftBottomCorner[0];
        corners[7] = leftBottomCorner[1];
        return corners;
    }

    /**
     * 获取SuperTextView的详细圆角信息，共4个圆角，每个圆角包含两个值。
     *
     * @return 返回SuperTextView的圆角信息 {@link SuperTextView#getCorners(float)}.
     */
    public float[] getCorners() {
        return corners;
    }


    private LinearShader createShader(int startColor, int endColor, ShaderMode shaderMode,
                                      float x0, float y0, float x1, float y1) {
        if (startColor != 0 && endColor != 0) {
            int temp = 0;
            switch (shaderMode) {
                case TOP_TO_BOTTOM:
                    x1 = x0;
                    break;
                case BOTTOM_TO_TOP:
                    x1 = x0;
                    temp = startColor;
                    startColor = endColor;
                    endColor = temp;
                    break;
                case LEFT_TO_RIGHT:
                    y1 = y0;
                    break;
                case RIGHT_TO_LEFT:
                    y1 = y0;
                    temp = startColor;
                    startColor = endColor;
                    endColor = temp;
                    break;
            }
            return new LinearShader(new Point[]{new Point(x0, x1)}, new float[]{x1, y1}, new Color[]{new Color(startColor), new Color(endColor)}, Shader.TileMode.CLAMP_TILEMODE);
        } else {
            return null;
        }
    }


    /**
     * SuperTextView的渐变模式。
     * 可以通过 {@link SuperTextView 设置控件的Shader模式}
     */
    public static enum ShaderMode {
        /**
         * 从上到下
         */
        TOP_TO_BOTTOM(0),
        /**
         * 从下到上
         */
        BOTTOM_TO_TOP(1),
        /**
         * 从左到右
         */
        LEFT_TO_RIGHT(2),
        /**
         * 从右到左
         */
        RIGHT_TO_LEFT(3);

        public int code;

        ShaderMode(int code) {
            this.code = code;
        }

        public static ShaderMode valueOf(int code) {
            for (ShaderMode mode : ShaderMode.values()) {
                if (mode.code == code) {
                    return mode;
                }
            }
            return TOP_TO_BOTTOM;
        }
    }

    private void drawStateDrawable(Canvas canvas) {
        HiLog.info(label, "drawStateDrawable");
        if (songDrawable != null) {
            if (drawableAsBackground) {
                long startDrawDrawableBackgroundTime = System.currentTimeMillis();
                drawDrawableBackground(canvas);
                Tracker.notifyEvent(tracker, TimeEvent.create(Event.OnDrawDrawableBackgroundEnd, System.currentTimeMillis() - startDrawDrawableBackgroundTime));
            } else if (isShowState) {
                getDrawableBounds();
                songDrawable.setBounds((int) drawableBounds[0], (int) drawableBounds[1], (int) drawableBounds[2], (int) drawableBounds[3]);
                if (drawableTint != NO_COLOR) {
                    RgbColor rgbColor = RgbColor.fromArgbInt(drawableTint);
                    ColorMatrix colorMatrix = new ColorMatrix();
                    float[] srcColor = new float[]{
                            1, 0, 0, 0, rgbColor.getRed(),
                            0, 1, 0, 0, rgbColor.getGreen(),
                            0, 0, 1, 0, rgbColor.getBlue(),
                            0, 0, 0, 1, rgbColor.getAlpha()};
                    colorMatrix.setMatrix(srcColor);
                    songDrawable.setColorMatrix(colorMatrix);
                    songDrawable.setStateColorMode(BlendMode.SRC_IN);
                }

                if (drawableRotate != NO_ROTATE) {
                    canvas.save();
                    canvas.rotate(drawableRotate,
                            drawableBounds[0] + (drawableBounds[2] - drawableBounds[0]) / 2,
                            drawableBounds[1] + (drawableBounds[3] - drawableBounds[1]) / 2);

                    songDrawable.drawToCanvas(canvas);
                    canvas.restore();
                } else {
                    HiLog.info(label, "drawToCanvas");
                    songDrawable.drawToCanvas(canvas);
                }
            }
        }
    }

    private void drawDrawableBackground(Canvas canvas) {
        if (songDrawable instanceof PixelMapElement) {
            PixelMapElement pixelMapElement = (PixelMapElement) songDrawable;
            HiLog.info(label, "drawDrawableBackground");
            HiLog.info(label, "component width:%{public}d,height:%{public}d", width, height);
            PixelMap pixelMap = pixelMapElement.getPixelMap();

            suitedSize = computeSuitedBitmapSize(pixelMapElement);
            PixelMapHolder pixelMapHolder = new PixelMapHolder(pixelMap);
            // 绘制图片，使用之前计算的数据
            PixelMapShader bitmapShader = new PixelMapShader(pixelMapHolder, Shader.TileMode.CLAMP_TILEMODE, Shader.TileMode.CLAMP_TILEMODE);
            initPaint();
            paint.setShader(bitmapShader, Paint.ShaderType.PIXELMAP_SHADER);
            paint.setStyle(Paint.Style.FILL_STYLE);
            RectFloat rectFloat = new RectFloat(0, 0, suitedSize[0], suitedSize[1]);

            canvas.drawRoundRect(rectFloat, corner, corner, paint);
            initPaint();
            paint.setStrokeWidth(strokeWidth);
            paint.setColor(new Color(strokeColor));
            paint.setStyle(Paint.Style.STROKE_STYLE);
            rectFloat.modify(0, 0, suitedSize[0], suitedSize[1]);
            canvas.drawRoundRect(rectFloat, corner, corner, paint);
        }

    }

    private void drawStateDrawable2(Canvas canvas) {
        if (drawable2 != null && isShowState2) {
            getDrawable2Bounds();
            drawable2.setBounds((int) drawable2Bounds[0], (int) drawable2Bounds[1], (int) drawable2Bounds[2], (int) drawable2Bounds[3]);
            if (drawable2Tint != NO_COLOR) {
                RgbColor rgbColor = RgbColor.fromArgbInt(drawable2Tint);
                ColorMatrix colorMatrix = new ColorMatrix();
                float[] srcColor = new float[]{
                        1, 0, 0, 0, rgbColor.getRed(),
                        0, 1, 0, 0, rgbColor.getGreen(),
                        0, 0, 1, 0, rgbColor.getBlue(),
                        0, 0, 0, 1, rgbColor.getAlpha()};
                colorMatrix.setMatrix(srcColor);
                drawable2.setColorMatrix(colorMatrix);
                drawable2.setStateColorMode(BlendMode.SRC_IN);
            }
            if (drawable2Rotate != NO_ROTATE) {
                canvas.save();
                canvas.rotate(drawable2Rotate,
                        drawable2Bounds[0] + (drawable2Bounds[2] - drawable2Bounds[0]) / 2,
                        drawable2Bounds[1] + (drawable2Bounds[3] - drawable2Bounds[1]) / 2);
                drawable2.drawToCanvas(canvas);
                canvas.restore();
            } else {
                drawable2.drawToCanvas(canvas);
            }
        }
    }

    private int[] computeSuitedBitmapSize(Element drawable) {
        int dWidth = drawable.getWidth();
        int dHeight = drawable.getWidth();
        if (!(dWidth > 0)) {
            dWidth = width;
        }
        if (!(dHeight > 0)) {
            dHeight = height;
        }
        int suitedWidth = width;
        int suitedHeight = height;
        if (suitedSize == null) {
            suitedSize = new int[4];
        }
        if (backgroundScaleType == ScaleType.FIT_CENTER) {
            if ((float) dWidth / (float) width > (float) dHeight / (float) height) {
                suitedHeight = (int) ((float) suitedWidth / ((float) dWidth / (float) dHeight));
            } else {
                suitedWidth = (int) (((float) dWidth / (float) dHeight) * (float) suitedHeight);
            }
            suitedSize[0] = suitedWidth;
            suitedSize[1] = suitedHeight;
            suitedSize[2] = width / 2 - suitedSize[0] / 2;
            suitedSize[3] = height / 2 - suitedSize[1] / 2;
        } else if (backgroundScaleType == ScaleType.FIT_XY) {
            suitedSize[0] = suitedWidth;
            suitedSize[1] = suitedHeight;
            suitedSize[2] = 0;
            suitedSize[3] = 0;
        } else {
            if ((float) dWidth / (float) width > (float) dHeight / (float) height) {
                suitedWidth = (int) (((float) dWidth / (float) dHeight) * (float) suitedHeight);
            } else {
                suitedHeight = (int) ((float) suitedWidth / ((float) dWidth / (float) dHeight));
            }
            suitedSize[0] = suitedWidth;
            suitedSize[1] = suitedHeight;
            suitedSize[2] = -(suitedSize[0] / 2 - width / 2);
            suitedSize[3] = -(suitedSize[1] / 2 - height / 2);
        }
        return suitedSize;
    }

    private float[] getDrawableBounds() {
        for (int i = 0; i < drawableBounds.length; i++) {
            drawableBounds[i] = 0;
        }
        drawableWidth = (drawableWidth == 0 ? width / 2f : drawableWidth);
        drawableHeight = (drawableHeight == 0 ? height / 2f : drawableHeight);
        switch (stateDrawableMode) {
            case "left": // left
                drawableBounds[0] = 0 + drawablePaddingLeft;
                drawableBounds[1] = height / 2f - drawableHeight / 2f + drawablePaddingTop;
                drawableBounds[2] = drawableBounds[0] + drawableWidth;
                drawableBounds[3] = drawableBounds[1] + drawableHeight;
                break;
            case "top": // top
                drawableBounds[0] = width / 2f - drawableWidth / 2f + drawablePaddingLeft;
                drawableBounds[1] = 0 + drawablePaddingTop;
                drawableBounds[2] = drawableBounds[0] + drawableWidth;
                drawableBounds[3] = drawableBounds[1] + drawableHeight;
                break;
            case "right": // right
                drawableBounds[0] = width - drawableWidth + drawablePaddingLeft;
                drawableBounds[1] = height / 2 - drawableHeight / 2 + drawablePaddingTop;
                drawableBounds[2] = drawableBounds[0] + drawableWidth;
                drawableBounds[3] = drawableBounds[1] + drawableHeight;
                break;
            case "bottom": // bottom
                drawableBounds[0] = width / 2f - drawableWidth / 2f + drawablePaddingLeft;
                drawableBounds[1] = height - drawableHeight + drawablePaddingTop;
                drawableBounds[2] = drawableBounds[0] + drawableWidth;
                drawableBounds[3] = drawableBounds[1] + drawableHeight;
                break;
            case "center": // center
                drawableBounds[0] = width / 2f - drawableWidth / 2f + drawablePaddingLeft;
                drawableBounds[1] = height / 2 - drawableHeight / 2 + drawablePaddingTop;
                drawableBounds[2] = drawableBounds[0] + drawableWidth;
                drawableBounds[3] = drawableBounds[1] + drawableHeight;
                break;
            case "fill": // fill
                drawableBounds[0] = 0;
                drawableBounds[1] = 0;
                drawableBounds[2] = width;
                drawableBounds[3] = height;
                break;
            case "left_top": // leftTop
                drawableBounds[0] = 0 + drawablePaddingLeft;
                drawableBounds[1] = 0 + drawablePaddingTop;
                drawableBounds[2] = drawableBounds[0] + drawableWidth;
                drawableBounds[3] = drawableBounds[1] + drawableHeight;
                break;
            case "right_top": // rightTop
                drawableBounds[0] = width - drawableWidth + drawablePaddingLeft;
                drawableBounds[1] = 0 + drawablePaddingTop;
                drawableBounds[2] = drawableBounds[0] + drawableWidth;
                drawableBounds[3] = drawableBounds[1] + drawableHeight;
                break;
            case "left_bottom": // leftBottom
                drawableBounds[0] = 0 + drawablePaddingLeft;
                drawableBounds[1] = height - drawableHeight + drawablePaddingTop;
                drawableBounds[2] = drawableBounds[0] + drawableWidth;
                drawableBounds[3] = drawableBounds[1] + drawableHeight;
                break;
            case "right_bottom": // rightBottom
                drawableBounds[0] = width - drawableWidth + drawablePaddingLeft;
                drawableBounds[1] = height - drawableHeight + drawablePaddingTop;
                drawableBounds[2] = drawableBounds[0] + drawableWidth;
                drawableBounds[3] = drawableBounds[1] + drawableHeight;
                break;
        }

        return drawableBounds;
    }

    private float[] getDrawable2Bounds() {
        for (int i = 0; i < drawable2Bounds.length; i++) {
            drawable2Bounds[i] = 0;
        }
        drawable2Width = (drawable2Width == 0 ? width / 2f : drawable2Width);
        drawable2Height = (drawable2Height == 0 ? height / 2f : drawable2Height);
        switch (stateDrawable2Mode) {
            case "left": // left
                drawable2Bounds[0] = 0 + drawable2PaddingLeft;
                drawable2Bounds[1] = height / 2f - drawable2Height / 2f + drawable2PaddingTop;
                drawable2Bounds[2] = drawable2Bounds[0] + drawable2Width;
                drawable2Bounds[3] = drawable2Bounds[1] + drawable2Height;
                break;
            case "top": // top
                drawable2Bounds[0] = width / 2f - drawable2Width / 2f + drawable2PaddingLeft;
                drawable2Bounds[1] = 0 + drawable2PaddingTop;
                drawable2Bounds[2] = drawable2Bounds[0] + drawable2Width;
                drawable2Bounds[3] = drawable2Bounds[1] + drawable2Height;
                break;
            case "right": // right
                drawable2Bounds[0] = width - drawable2Width + drawable2PaddingLeft;
                drawable2Bounds[1] = height / 2 - drawable2Height / 2 + drawable2PaddingTop;
                drawable2Bounds[2] = drawable2Bounds[0] + drawable2Width;
                drawable2Bounds[3] = drawable2Bounds[1] + drawable2Height;
                break;
            case "bottom": // bottom
                drawable2Bounds[0] = width / 2f - drawable2Width / 2f + drawable2PaddingLeft;
                drawable2Bounds[1] = height - drawable2Height + drawable2PaddingTop;
                drawable2Bounds[2] = drawable2Bounds[0] + drawable2Width;
                drawable2Bounds[3] = drawable2Bounds[1] + drawable2Height;
                break;
            case "center": // center
                drawable2Bounds[0] = width / 2f - drawable2Width / 2f + drawable2PaddingLeft;
                drawable2Bounds[1] = height / 2 - drawable2Height / 2 + drawable2PaddingTop;
                drawable2Bounds[2] = drawable2Bounds[0] + drawable2Width;
                drawable2Bounds[3] = drawable2Bounds[1] + drawable2Height;
                break;
            case "fill": // fill
                drawable2Bounds[0] = 0;
                drawable2Bounds[1] = 0;
                drawable2Bounds[2] = width;
                drawable2Bounds[3] = height;
                break;
            case "left_top": // leftTop
                drawable2Bounds[0] = 0 + drawable2PaddingLeft;
                drawable2Bounds[1] = 0 + drawable2PaddingTop;
                drawable2Bounds[2] = drawable2Bounds[0] + drawable2Width;
                drawable2Bounds[3] = drawable2Bounds[1] + drawable2Height;
                break;
            case "right_top": // rightTop
                drawable2Bounds[0] = width - drawable2Width + drawable2PaddingLeft;
                drawable2Bounds[1] = 0 + drawable2PaddingTop;
                drawable2Bounds[2] = drawable2Bounds[0] + drawable2Width;
                drawable2Bounds[3] = drawable2Bounds[1] + drawable2Height;
                break;
            case "left_botom": // leftBottom
                drawable2Bounds[0] = 0 + drawable2PaddingLeft;
                drawable2Bounds[1] = height - drawable2Height + drawable2PaddingTop;
                drawable2Bounds[2] = drawable2Bounds[0] + drawable2Width;
                drawable2Bounds[3] = drawable2Bounds[1] + drawable2Height;
                break;
            case "right_bottom": // rightBottom
                drawable2Bounds[0] = width - drawable2Width + drawable2PaddingLeft;
                drawable2Bounds[1] = height - drawable2Height + drawable2PaddingTop;
                drawable2Bounds[2] = drawable2Bounds[0] + drawable2Width;
                drawable2Bounds[3] = drawable2Bounds[1] + drawable2Height;
                break;
        }

        return drawable2Bounds;
    }


    private void isNeedToAdjust(Canvas canvas, Adjuster.Opportunity currentOpportunity) {
        for (int i = 0; i < adjusterList.size(); i++) {
            Adjuster adjuster = adjusterList.get(i);
            if (currentOpportunity == adjuster.getOpportunity()) {
                long startDrawAdjustersTime = System.currentTimeMillis();
                if (adjuster.getType() == Adjuster.TYPE_SYSTEM) {
                    adjuster.adjust(this, canvas);
                } else if (autoAdjust) {
                    adjuster.adjust(this, canvas);
                }
                Tracker.notifyEvent(tracker, TimeEvent.create(Event.OnDrawAdjustersEnd, System.currentTimeMillis() - startDrawAdjustersTime));
            }
        }
    }


    private void drawTextStroke(Canvas canvas) {
    }


    private void drawShaderText(Canvas canvas) {

    }


    /**
     * 获取SuperTextView的圆角大小
     *
     * @return 获取Corner值。默认为0。
     */
    public float getCorner() {
        return corner;
    }


    /**
     * 设置圆角大小
     *
     * @param corner 圆角大小，默认值为0。会触发一次重绘。
     * @return SuperTextView
     */
    public SuperTextView setCorner(float corner) {
        this.corner = corner;
        invalidate();

        return this;
    }

    /**
     * 获取填充颜色
     *
     * @return 返回控件填充颜色。
     */
    public int getSolid() {
        return solid;
    }

    /**
     * 设置填充颜色
     *
     * @param solid 控件填充颜色, 默认为{@link Color#TRANSPARENT}。会触发一次重绘。
     * @return SuperTextView
     */
    public SuperTextView setSolid(int solid) {
        this.solid = solid;
        invalidate();

        return this;
    }

    /**
     * 获取控件边框宽度
     *
     * @return 返回控件边框的宽度。
     */
    public float getStrokeWidth() {
        return strokeWidth;
    }

    /**
     * 设置控件边框宽度
     *
     * @param strokeWidth 描边宽度。会触发一次重绘。
     * @return SuperTextView
     */
    public SuperTextView setStrokeWidth(float strokeWidth) {
        this.strokeWidth = strokeWidth;
        invalidate();

        return this;
    }

    /**
     * 获取边框颜色
     *
     * @return 返回描边颜色。默认为{@link Color#BLACK}。
     */
    public int getStrokeColor() {
        return strokeColor;
    }

    /**
     * 设置边框颜色
     *
     * @param strokeColor 描边颜色。默认为{@link Color#BLACK}。会触发一次重绘。
     * @return SuperTextView
     */
    public SuperTextView setStrokeColor(int strokeColor) {
        this.strokeColor = strokeColor;
        invalidate();

        return this;
    }

    /**
     * 获取最后一个 {@link Adjuster}
     *
     * @return 获得最后一个 {@link Adjuster}，如果存在的话。
     */
    public Adjuster getAdjuster() {
        if (adjusterList.size() > SYSTEM_ADJUSTER_SIZE) {
            return adjusterList.get(adjusterList.size() - 1);
        }
        return null;
    }

    /**
     * 获得index对应的 {@link Adjuster}。
     *
     * @param index 期望获得的Adjuster的index。
     * @return index对应的Adjuster，如果参数错误返回null。
     */
    public Adjuster getAdjuster(int index) {
        int realIndex = SYSTEM_ADJUSTER_SIZE + index;
        if (realIndex > SYSTEM_ADJUSTER_SIZE - 1 && realIndex < adjusterList.size()) {
            return adjusterList.get(realIndex);
        }
        return null;
    }

    /**
     * 获得SuperTextView中的所有Adjuster，如果没有返回null
     *
     * @return 如果SuperTextView有Adjuster，返回List<Adjuster>；否则返回null
     */
    public List<Adjuster> getAdjusterList() {
        if (adjusterList.size() > SYSTEM_ADJUSTER_SIZE) {
            ArrayList<Adjuster> r = new ArrayList<>();
            r.addAll(SYSTEM_ADJUSTER_SIZE, adjusterList);
            return r;
        }
        return null;
    }

    /**
     * 添加一个Adjuster。
     * 注意，最多支持添加3个Adjuster，否则新的Adjuster总是会覆盖最后一个Adjuster。
     *
     * @param adjuster {@link Adjuster}。会触发一次重绘。
     * @return SuperTextView
     */
    public SuperTextView addAdjuster(Adjuster adjuster) {
        if (adjusterList.size() < SYSTEM_ADJUSTER_SIZE + ALLOW_CUSTOM_ADJUSTER_SIZE) {
            innerAddAdjuster(adjuster);
        } else {
            removeAdjuster(adjusterList.size() - 1);
            innerAddAdjuster(adjuster);
        }
        return this;
    }

    private void addSysAdjuster(Adjuster adjuster) {
        if (adjuster != null) {
            adjuster.setType(Adjuster.TYPE_SYSTEM);
            adjusterList.add(SYSTEM_ADJUSTER_SIZE, adjuster);
            SYSTEM_ADJUSTER_SIZE++;
        }
    }

    private void innerAddAdjuster(Adjuster adjuster) {
        adjusterList.add(adjuster);
        adjuster.attach(this);
        invalidate();
    }

    /**
     * 移除指定位置的Adjuster。
     *
     * @param index 期望移除的Adjuster的位置。
     * @return 被移除的Adjuster，如果参数错误返回null。
     */
    public Adjuster removeAdjuster(int index) {
        int realIndex = SYSTEM_ADJUSTER_SIZE + index;
        if (realIndex > SYSTEM_ADJUSTER_SIZE - 1 && realIndex < adjusterList.size()) {
            Adjuster remove = adjusterList.remove(realIndex);
            remove.detach(this);
            invalidate();
            return remove;
        }
        return null;
    }

    /**
     * 移除指定的Adjuster，如果包含的话。
     *
     * @param adjuster 需要被移除的Adjuster
     * @return 被移除Adjuster在移除前在Adjuster列表中的位置。如果没有包含，返回-1。
     */
    public int removeAdjuster(Adjuster adjuster) {
        if (adjuster.type != Adjuster.TYPE_SYSTEM && adjusterList.contains(adjuster)) {
            int index = adjusterList.indexOf(adjuster);
            adjusterList.remove(adjuster);
            adjuster.detach(this);
            invalidate();
            return index;
        }
        return -1;
    }

    /**
     * 检查是否开启了文字描边
     *
     * @return true 表示开启了文字描边，否则表示没开启。
     */
    public boolean isTextStroke() {
        return textStroke;
    }

    /**
     * 设置是否开启文字描边。
     * 注意，开启文字描边后，文字颜色需要通过 {@link #setTextFillColor(int)} 设置。
     *
     * @param textStroke true表示开启文字描边。默认为false。会触发一次重绘。
     * @return SuperTextView
     */
    public SuperTextView setTextStroke(boolean textStroke) {
        this.textStroke = textStroke;
        invalidate();

        return this;
    }

    /**
     * 获取文字描边的颜色
     *
     * @return 文字描边的颜色。
     */
    public int getTextStrokeColor() {
        return textStrokeColor;
    }

    /**
     * 设置文字描边的颜色
     *
     * @param textStrokeColor 设置文字描边的颜色。默认为{@link Color#BLACK}。会触发一次重绘。
     * @return SuperTextView
     */
    public SuperTextView setTextStrokeColor(int textStrokeColor) {
        this.textStrokeColor = textStrokeColor;
        invalidate();

        return this;
    }

    /**
     * 获取文字的填充颜色，在开启文字描边时 {@link #setTextStroke(boolean)} 默认为BLACK。
     *
     * @return 文字填充颜色。
     */
    public int getTextFillColor() {
        return textFillColor;
    }

    /**
     * 设置文字的填充颜色，需要开启文字描边 {@link #setTextStroke(boolean)} 才能生效。默认为BLACK。
     *
     * @param textFillColor 设置文字填充颜色。默认为{@link Color#BLACK}。会触发一次重绘。
     * @return SuperTextView
     */
    public SuperTextView setTextFillColor(int textFillColor) {
        this.textFillColor = textFillColor;
        invalidate();

        return this;
    }

    /**
     * 获取文字描边的宽度
     *
     * @return 文字描边宽度。
     */
    public float getTextStrokeWidth() {
        return textStrokeWidth;
    }

    /**
     * 设置文字描边的宽度，需要开启文字描边 {@link #setTextStroke(boolean)} 才能生效。
     *
     * @param textStrokeWidth 设置文字描边宽度。会触发一次重绘。
     * @return SuperTextView
     */
    public SuperTextView setTextStrokeWidth(float textStrokeWidth) {
        this.textStrokeWidth = textStrokeWidth;
        invalidate();

        return this;
    }

    /**
     * 检查是否开启 {@link Adjuster} 功能。
     *
     * @return true表示开启了Adjuster功能。
     */
    public boolean isAutoAdjust() {
        return autoAdjust;
    }

    /**
     * 设置是否开启 {@link Adjuster} 功能。
     *
     * @param autoAdjust true开启Adjuster功能。反之，关闭。会触发一次重绘。
     * @return SuperTextView
     */
    public SuperTextView setAutoAdjust(boolean autoAdjust) {
        this.autoAdjust = autoAdjust;
        invalidate();

        return this;
    }

    /**
     * 检查左上角是否设置成了圆角。
     *
     * @return true表示左上角为圆角。
     */
    public boolean isLeftTopCornerEnable() {
        return leftTopCornerEnable;
    }

    /**
     * 设置左上角是否为圆角，可以单独控制SuperTextView的每一个圆角。
     *
     * @param leftTopCornerEnable true左上角圆角化。会触发一次重绘。
     * @return SuperTextView
     */
    public SuperTextView setLeftTopCornerEnable(boolean leftTopCornerEnable) {
        this.leftTopCornerEnable = leftTopCornerEnable;
        invalidate();

        return this;
    }

    /**
     * 检查右上角是否设置成了圆角。
     *
     * @return true表示右上角为圆角。
     */
    public boolean isRightTopCornerEnable() {
        return rightTopCornerEnable;
    }

    /**
     * 设置右上角是否为圆角，可以单独控制SuperTextView的每一个圆角。
     *
     * @param rightTopCornerEnable true右上角圆角化。会触发一次重绘。
     * @return SuperTextView
     */
    public SuperTextView setRightTopCornerEnable(boolean rightTopCornerEnable) {
        this.rightTopCornerEnable = rightTopCornerEnable;
        invalidate();

        return this;
    }

    /**
     * 检查左下角是否设置成了圆角。
     *
     * @return true表示左下角为圆角。
     */
    public boolean isLeftBottomCornerEnable() {
        return leftBottomCornerEnable;
    }

    /**
     * 设置左下角是否为圆角，可以单独控制SuperTextView的每一个圆角。
     *
     * @param leftBottomCornerEnable true左下角圆角化。会触发一次重绘。
     * @return SuperTextView
     */
    public SuperTextView setLeftBottomCornerEnable(boolean leftBottomCornerEnable) {
        this.leftBottomCornerEnable = leftBottomCornerEnable;
        invalidate();

        return this;
    }

    /**
     * 检查右下角是否设置成了圆角。
     *
     * @return true表示右下角为圆角。
     */
    public boolean isRightBottomCornerEnable() {
        return rightBottomCornerEnable;
    }

    /**
     * 设置右下角是否为圆角，可以单独控制SuperTextView的每一个圆角。
     *
     * @param rightBottomCornerEnable true右下角圆角化。会触发一次重绘。
     * @return SuperTextView
     */
    public SuperTextView setRightBottomCornerEnable(boolean rightBottomCornerEnable) {
        this.rightBottomCornerEnable = rightBottomCornerEnable;
        invalidate();

        return this;
    }

    /**
     * 获取状态图，当状态图通过 {@link #setDrawableAsBackground(boolean)} 设置为背景时，获取的就是背景图了。
     *
     * @return 状态图。
     */
    public Element getDrawable() {
        return songDrawable;
    }

    /**
     * 获取状态图2号。
     *
     * @return 状态图2。
     */
    public Element getDrawable2() {
        return drawable2;
    }

    /**
     * 设置状态图。需要调用 {@link #setShowState(boolean)} 才能显示。会触发一次重绘。
     * 当通过 {@link #setDrawableAsBackground(boolean)} 将状态图设置为背景后，将会被作为SuperTextView的背景图。
     * 通过 {@link #isDrawableAsBackground()} 来检查状态图是否被设置成了背景。
     *
     * @param songDrawable 状态图。
     * @return SuperTextView
     */
    public SuperTextView setSongDrawable(Element songDrawable) {
        this.songDrawable = songDrawable;
        drawableBackgroundShader = null;
//        invalidate();
        setBackground(songDrawable);
        return this;
    }


    /**
     * 检查是否显示状态图
     *
     * @return 返回true，如果当前显示状态图。
     */
    public boolean isShowState() {
        return isShowState;
    }

    /**
     * 设置是否显示状态图。
     *
     * @param showState true，表示显示状态图。反之，不显示。会触发一次重绘。
     * @return SuperTextView
     */
    public SuperTextView setShowState(boolean showState) {
        isShowState = showState;
        invalidate();

        return this;
    }

    /**
     * 检查当前状态图是否被作为SuperTextView的背景图。
     *
     * @return 当前Drawable是否作为SuperTextView的背景图。
     */
    public boolean isDrawableAsBackground() {
        return drawableAsBackground;
    }


    /**
     * 设置是否将状态图作为SuperTextView的背景图。
     * 将状态图设置为背景图，可以将SuperTextView变成一个ImageView用来展示图片，对SuperTextView设置的圆角、边框仍然对图片
     * 有效，这对于需要实现圆形图片、给图片加边框很有帮助。而且通过 {@link #setUrlImage(String)} 和 {@link #setUrlImage(String, boolean)}
     * 可以使得SuperTextView能够自动下载网络图片，然后进行展示。
     *
     * @param drawableAsBackground true，表示将Drawable作为背景图，其余所有对drawable的设置都会失效，直到该项为false。
     * @return SuperTextView
     */
    public SuperTextView setDrawableAsBackground(boolean drawableAsBackground) {
        this.drawableAsBackground = drawableAsBackground;
        if (!drawableAsBackground) {
            drawableBackgroundShader = null;
        }
        invalidate();
        return this;
    }

    /**
     * 检查是否显示状态图2。
     *
     * @return 返回true，如果当前显示状态图2。
     */
    public boolean isShowState2() {
        return isShowState2;
    }


    /**
     * 设置是否显示状态图2
     *
     * @param showState true，表示显示状态图2。反之，不显示。会触发一次重绘。
     * @return SuperTextView
     */
    public SuperTextView setShowState2(boolean showState) {
        isShowState2 = showState;
        invalidate();

        return this;
    }

    /**
     * 获得Drawable1的层级
     *
     * @return
     */
    public int getStateDrawableLayer() {
        return stateDrawableLayer;
    }

    /**
     * 设置Drawable1的层级
     *
     * @param drawableLayer
     * @return
     */
    public SuperTextView setStateDrawableLayer(int drawableLayer) {
        this.stateDrawableLayer = drawableLayer;
        invalidate();

        return this;
    }

    /**
     * 获得Drawable2的层级
     *
     * @return
     */
    public int getStateDrawable2Layer() {
        return stateDrawable2Layer;
    }

    /**
     * 设置Drawable2的层级
     *
     * @param drawableLayer
     * @return
     */
    public SuperTextView setStateDrawable2Layer(int drawableLayer) {
        this.stateDrawable2Layer = drawableLayer;
        invalidate();

        return this;
    }

    /**
     * 获取状态图的显示模式。在 {@link DrawableMode} 中查看所有支持的模式。
     *
     * @return 状态图显示模式。{@link DrawableMode}
     */
    public String getStateDrawableMode() {
        return stateDrawableMode;
    }

    /**
     * 获取状态图2的显示模式。在 {@link DrawableMode} 中查看所有支持的模式。
     *
     * @return 状态图2显示模式。{@link DrawableMode}
     */
    public String getStateDrawable2Mode() {
        return stateDrawable2Mode;
    }

    /**
     * 设置状态图显示模式。默认为{@link DrawableMode#CENTER}。会触发一次重绘。
     * 在 {@link DrawableMode} 中查看所有支持的模式。
     *
     * @param stateDrawableMode 状态图显示模式
     * @return SuperTextView
     */
    public SuperTextView setStateDrawableMode(String stateDrawableMode) {
        this.stateDrawableMode = stateDrawableMode;
        invalidate();

        return this;
    }

    /**
     * 设置状态图2显示模式。默认为{@link DrawableMode#CENTER}。会触发一次重绘。
     * 在 {@link DrawableMode} 中查看所有支持的模式。
     *
     * @param stateDrawableMode 状态图2显示模式
     * @return SuperTextView
     */
    public SuperTextView setStateDrawable2Mode(String stateDrawableMode) {
        this.stateDrawable2Mode = stateDrawableMode;
        invalidate();

        return this;
    }

    /**
     * 获取状态图的宽度。
     *
     * @return 状态图的宽度。
     */
    public float getDrawableWidth() {
        return drawableWidth;
    }

    /**
     * 获取状态图2的宽度。
     *
     * @return 状态图2的宽度。
     */
    public float getDrawable2Width() {
        return drawable2Width;
    }

    /**
     * 设置状态图宽度。默认为控件的1／2。会触发一次重绘。
     *
     * @param drawableWidth 状态图宽度
     * @return SuperTextView
     */
    public SuperTextView setDrawableWidth(float drawableWidth) {
        this.drawableWidth = drawableWidth;
        invalidate();

        return this;
    }

    /**
     * 设置状态图2宽度。默认为控件的1／2。会触发一次重绘。
     *
     * @param drawableWidth 状态图2宽度
     * @return SuperTextView
     */
    public SuperTextView setDrawable2Width(float drawableWidth) {
        this.drawable2Width = drawableWidth;
        invalidate();

        return this;
    }

    /**
     * 获取状态图的高度
     *
     * @return 状态图的高度。
     */
    public float getDrawableHeight() {
        return drawableHeight;
    }

    /**
     * 获取状态图2的高度
     *
     * @return 状态图2的高度。
     */
    public float getDrawable2Height() {
        return drawable2Height;
    }

    /**
     * 设置状态图高度。默认为控件的1／2。会触发一次重绘。
     *
     * @param drawableHeight 状态图高度
     * @return SuperTextView
     */
    public SuperTextView setDrawableHeight(float drawableHeight) {
        this.drawableHeight = drawableHeight;
        invalidate();

        return this;
    }

    /**
     * 设置状态图2高度。默认为控件的1／2。会触发一次重绘。
     *
     * @param drawableHeight 状态图2高度
     * @return SuperTextView
     */
    public SuperTextView setDrawable2Height(float drawableHeight) {
        this.drawable2Height = drawableHeight;
        invalidate();

        return this;
    }

    /**
     * 获取状态图相对于控件左边的边距。
     *
     * @return 状态图左边距
     */
    public float getDrawablePaddingLeft() {
        return drawablePaddingLeft;
    }

    /**
     * 获取状态图2相对于控件左边的边距。
     *
     * @return 状态图2左边距
     */
    public float getDrawable2PaddingLeft() {
        return drawable2PaddingLeft;
    }

    /**
     * 设置状态图相对于控件左边的边距。会触发一次重绘。
     *
     * @param drawablePaddingLeft 状态图左边距。
     * @return SuperTextView
     */
    public SuperTextView setDrawablePaddingLeft(float drawablePaddingLeft) {
        this.drawablePaddingLeft = drawablePaddingLeft;
        invalidate();

        return this;
    }

    /**
     * 设置状态图2相对于控件左边的边距。会触发一次重绘。
     *
     * @param drawablePaddingLeft 状态图左边距。
     * @return SuperTextView
     */
    public SuperTextView setDrawable2PaddingLeft(float drawablePaddingLeft) {
        this.drawable2PaddingLeft = drawablePaddingLeft;
        invalidate();

        return this;
    }

    /**
     * 获取状态图相对于控件上边的边距。
     *
     * @return 状态图上边距。
     */
    public float getDrawablePaddingTop() {
        return drawablePaddingTop;
    }

    /**
     * 获取状态图2相对于控件上边的边距。
     *
     * @return 状态图2上边距。
     */
    public float getDrawable2PaddingTop() {
        return drawable2PaddingTop;
    }

    /**
     * 设置状态图相对于控件上边的边距。会触发一次重绘。
     *
     * @param drawablePaddingTop 状态图上边距。
     * @return SuperTextView
     */
    public SuperTextView setDrawablePaddingTop(float drawablePaddingTop) {
        this.drawablePaddingTop = drawablePaddingTop;
        invalidate();
        return this;
    }

    /**
     * 设置状态图2相对于控件上边的边距。会触发一次重绘。
     *
     * @param drawablePaddingTop 状态图2上边距。
     * @return SuperTextView
     */
    public SuperTextView setDrawable2PaddingTop(float drawablePaddingTop) {
        this.drawable2PaddingTop = drawablePaddingTop;
        invalidate();
        return this;
    }

    /**
     * 设置第一个状态图的混合颜色。可以修改原本的drawable的颜色。
     * <p>
     * 如果需要还原为原来的颜色只需要设置颜色为 {@link SuperTextView#NO_COLOR}.
     *
     * @param tintColor 目标混合颜色
     * @return
     */
    public SuperTextView setDrawableTint(int tintColor) {
        this.drawableTint = tintColor;
        invalidate();
        return this;
    }

    /**
     * 获得第一个状态图的混合颜色。
     * <p>
     * 默认为 {@link SuperTextView#NO_COLOR}
     *
     * @return
     */
    public int getDrawableTint() {
        return drawableTint;
    }


    /**
     * 设置第二个状态图的混合颜色。可以修改原本的drawable的颜色。
     * <p>
     * 如果需要还原为原来的颜色只需要设置颜色为 {@link SuperTextView#NO_COLOR}.
     *
     * @param tintColor 目标混合颜色
     * @return
     */
    public SuperTextView setDrawable2Tint(int tintColor) {
        this.drawable2Tint = tintColor;
        invalidate();
        return this;
    }

    /**
     * 获得第二个状态图的混合颜色。
     * <p>
     * 默认为 {@link SuperTextView#NO_COLOR}
     *
     * @return
     */
    public int getDrawable2Tint() {
        return drawable2Tint;
    }

    /**
     * 设置第一个状态图的旋转角度。
     * <p>
     * 如果需要恢复默认角度只需要设置为 {@link SuperTextView#NO_ROTATE}.
     *
     * @param rotate 需要旋转的角度
     * @return
     */
    public SuperTextView setDrawableRotate(float rotate) {
        this.drawableRotate = rotate;
        invalidate();
        return this;
    }

    /**
     * 获得第一个状态图的旋转角度。
     * <p>
     * 默认为 {@link SuperTextView#NO_ROTATE}
     *
     * @return
     */
    public float getDrawableRotate() {
        return drawableRotate;
    }

    /**
     * 设置第二个状态图的旋转角度。
     * <p>
     * 如果需要恢复默认角度只需要设置为 {@link SuperTextView#NO_ROTATE}.
     *
     * @param rotate 需要旋转的角度
     * @return
     */
    public SuperTextView setDrawable2Rotate(float rotate) {
        this.drawable2Rotate = rotate;
        invalidate();
        return this;
    }

    /**
     * 获得第二个状态图的旋转角度。
     * <p>
     * 默认为 {@link SuperTextView#NO_ROTATE}
     *
     * @return
     */
    public float getDrawable2Rotate() {
        return drawable2Rotate;
    }


    /**
     * 获取渐变色的起始颜色。
     *
     * @return 渐变起始色。
     */
    public int getShaderStartColor() {
        return shaderStartColor;
    }

    /**
     * 设置渐变起始色。需要调用{@link SuperTextView#setShaderEnable(boolean)}后才能生效。会触发一次重绘。
     *
     * @param shaderStartColor 渐变起始色
     * @return SuperTextView
     */
    public SuperTextView setShaderStartColor(int shaderStartColor) {
        this.shaderStartColor = shaderStartColor;
        solidShader = null;
        invalidate();
        return this;
    }

    /**
     * 获取渐变色的结束颜色。
     *
     * @return 渐变结束色。
     */
    public int getShaderEndColor() {
        return shaderEndColor;
    }

    /**
     * 设置渐变结束色。需要调用{@link SuperTextView#setShaderEnable(boolean)}后才能生效。会触发一次重绘。
     *
     * @param shaderEndColor 渐变结束色
     * @return SuperTextView
     */
    public SuperTextView setShaderEndColor(int shaderEndColor) {
        this.shaderEndColor = shaderEndColor;
        solidShader = null;
        invalidate();
        return this;
    }

    /**
     * 获取渐变色模式。在{@link ShaderMode}中可以查看所有支持的模式。
     * 需要调用{@link SuperTextView#setShaderEnable(boolean)}后才能生效。
     *
     * @return 渐变模式。
     */
    public String getShaderMode() {
        return shaderMode;
    }

    /**
     * 设置渐变模式。在{@link ShaderMode}中可以查看所有支持的模式。
     * 需要调用{@link SuperTextView#setShaderEnable(boolean)}后才能生效。
     *
     * @param shaderMode 渐变模式
     * @return SuperTextView
     */
    public SuperTextView setShaderMode(String shaderMode) {
        this.shaderMode = shaderMode;
        solidShader = null;
        invalidate();
        return this;
    }

    /**
     * 检查是否启用了渐变功能。
     *
     * @return 返回true，如果启用了渐变功能。
     */
    public boolean isShaderEnable() {
        return shaderEnable;
    }

    /**
     * 设置是否启用渐变色功能。
     *
     * @param shaderEnable true启用渐变功能。反之，停用。
     * @return SuperTextView
     */
    public SuperTextView setShaderEnable(boolean shaderEnable) {
        this.shaderEnable = shaderEnable;
        invalidate();
        return this;
    }

    /**
     * 获取文字渐变色的起始颜色。
     *
     * @return 文字渐变起始色。
     */
    public int getTextShaderStartColor() {
        return textShaderStartColor;
    }

    /**
     * 设置文字渐变起始色。需要调用{@link SuperTextView#setTextShaderEnable(boolean)}后才能生效。会触发一次重绘。
     *
     * @param shaderStartColor 文字渐变起始色
     * @return SuperTextView
     */
    public SuperTextView setTextShaderStartColor(int shaderStartColor) {
        this.textShaderStartColor = shaderStartColor;
        textShader = null;
        invalidate();
        return this;
    }

    /**
     * 获取文字渐变色的结束颜色。
     *
     * @return 文字渐变结束色。
     */
    public int getTextShaderEndColor() {
        return textShaderEndColor;
    }

    /**
     * 设置文字渐变结束色。需要调用{@link SuperTextView#setShaderEnable(boolean)}后才能生效。会触发一次重绘。
     *
     * @param shaderEndColor 文字渐变结束色
     * @return SuperTextView
     */
    public SuperTextView setTextShaderEndColor(int shaderEndColor) {
        this.textShaderEndColor = shaderEndColor;
        textShader = null;
        invalidate();
        return this;
    }

    /**
     * 获取文字渐变色模式。在{@link ShaderMode}中可以查看所有支持的模式。
     * 需要调用{@link SuperTextView#setTextShaderEnable(boolean)}后才能生效。
     *
     * @return 渐变模式。
     */
    public ShaderMode getTextShaderMode() {
        return textShaderMode;
    }

    /**
     * 设置文字渐变模式。在{@link ShaderMode}中可以查看所有支持的模式。
     * 需要调用{@link SuperTextView#setTextShaderEnable(boolean)}后才能生效。
     *
     * @param shaderMode 文字渐变模式
     * @return SuperTextView
     */
    public SuperTextView setTextShaderMode(ShaderMode shaderMode) {
        this.textShaderMode = shaderMode;
        textShader = null;
        invalidate();
        return this;
    }

    /**
     * 检查是否启用了文字渐变功能。
     *
     * @return 返回true，如果启用了文字渐变功能。
     */
    public boolean isTextShaderEnable() {
        return textShaderEnable;
    }

    /**
     * 设置是否启用文字渐变色功能。
     *
     * @param shaderEnable true启用文字渐变功能。反之，停用。
     * @return SuperTextView
     */
    public SuperTextView setTextShaderEnable(boolean shaderEnable) {
        this.textShaderEnable = shaderEnable;
        invalidate();
        return this;
    }


    /**
     * 获得当前按压背景色。没有设置默认为Color.TRANSPARENT。
     *
     * @return 按压时的背景色
     */
    public int getPressBgColor() {
        return pressBgColor;
    }

    /**
     * 获得当前按压背景色。一旦设置，立即生效。
     * 取消可以设置Color.TRANSPARENT。
     *
     * @param pressBgColor 按压背景色
     */
    public SuperTextView setPressBgColor(int pressBgColor) {
        this.pressBgColor = pressBgColor;
        return this;
    }

    /**
     * 获得当前按压文字颜色色。没有设置默认为 {@link SuperTextView#NO_COLOR}。
     *
     * @return 按压时文字颜色
     */
    public int getPressTextColor() {
        return pressTextColor;
    }

    /**
     * 获得当前按压文字色。一旦设置，立即生效。
     * 取消可以设置 {@link SuperTextView#NO_COLOR}。
     *
     * @param pressTextColor 按压时文字颜色
     */
    public SuperTextView setPressTextColor(int pressTextColor) {
        this.pressTextColor = pressTextColor;
        return this;
    }

    /**
     * 获取当前SuperTextView在播放 {@link Adjuster} 时的帧率。默认为60fps
     *
     * @return 帧率
     */
    public int getFrameRate() {
        return frameRate;
    }

    /**
     * 设置帧率，即每秒帧数。可在动画过程中随时改变。默认为60fps
     *
     * @param frameRate 帧率
     * @return SuperTextView
     */
    public SuperTextView setFrameRate(int frameRate) {
        if (frameRate > 0) {
            this.frameRate = frameRate;
        } else {
            this.frameRate = 60;
        }
        return this;
    }

    /**
     * 将一个网络图片作为SuperTextView的StateDrawable。
     * 在调用这个函数前，建议开发者根据当前所使用的图片框架实现{@link }，
     * 然后通过{@link ImageEngine#}为SuperTextView的ImageEngine安装一个全局引擎，开发者可以在
     * {@link}中进行配置（需要注意任何时候新安装的引擎总会替换掉原本的引擎）。
     * 在未设置引擎的情况下，SuperTextView仍然会通过内置的一个十分简易引擎去下载图片。
     *
     * @param url          网络图片地址
     * @param asBackground 是否将下载的图片作为Background。
     *                     - false表示下载好的图片将作为SuperTextView的StateDrawable
     *                     - true表示将下载好的图片作为背景，效果和{@link SuperTextView#setDrawableAsBackground(boolean)}
     *                     是一样的。
     * @return SuperTextView
     */
    public SuperTextView setUrlImage(final String url, final boolean asBackground) {
        HiLog.info(label, "ImageUrl:%{public}s", url);
        // 检查是否已经安装了Engine，没有安装会安装一个默认的，后面仍然可以随时被替换
        ImageEngine.checkEngine();
        // 缓存当前的imageUrl，当下载完成后需要校验
        curImageUrl = url;
        ImageEngine.load(url, new ImageEngine.Callback() {
            @Override
            public void onCompleted(final PixelMapElement drawable) {
                if (getContext() != null && drawable != null && TextTool.isEqual(curImageUrl, url)) {
                    SuperTextView.this.drawableAsBackground = asBackground;
                    setSongDrawable(drawable);
                }
                if (drawable == null) {
                    HiLog.info(label, "network is fail");
                }
            }
        });
        return this;
    }

    /**
     * 将一个网络图片作为SuperTextView的StateDrawable，调用这个方法StateDrawable将会被设置为背景模式。
     * 在调用这个函数前，建议开发者根据当前所使用的图片框架实现{@link }，
     * 然后通过{@link ImageEngine#()}为SuperTextView的ImageEngine安装一个全局引擎，开发者可以在
     * {@link #()}中进行配置（需要注意任何时候新安装的引擎总会替换掉原本的引擎）。
     * 在未设置引擎的情况下，SuperTextView仍然会通过内置的一个十分简易引擎去下载图片。
     *
     * @param url 网络图片地址
     * @return SuperTextView
     */
    public SuperTextView setUrlImage(String url) {
        return setUrlImage(url, true);
    }


    /**
     * 启动动画。需要设置{@link SuperTextView#setAutoAdjust(boolean)}为true才能看到。
     */
    public void startAnim() {
        needRun = true;
        runnable = false;
        if (animThread == null) {
            checkWhetherNeedInitInvalidate();
            needRun = true;
            runnable = true;
            if (handleAnim == null) {
                initHandleAnim();
            }
            animThread = new Thread(handleAnim);
            animThread.start();
        }
    }

    private void initHandleAnim() {
        handleAnim = new Runnable() {
            @Override
            public void run() {
                while (runnable) {
                    synchronized (invalidate) {
                        getContext().getUITaskDispatcher().syncDispatch(() -> invalidate());
                    }
                    try {
                        Thread.sleep(1000 / frameRate);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        runnable = false;
                    }
                }
                animThread = null;
                if (needRun) {
                    startAnim();
                }
            }
        };
    }

    private void checkWhetherNeedInitInvalidate() {
        if (invalidate == null) {
            invalidate = new Runnable() {
                @Override
                public void run() {
                    getContext().getUITaskDispatcher().syncDispatch(() -> invalidate());
                }
            };
        }
    }

    /**
     * 停止动画。不能保证立即停止，但最终会停止。
     */
    public void stopAnim() {
        runnable = false;
        needRun = false;
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        int action = event.getAction();
        MmiPoint mmiPoint = event.getPointerPosition(0);
        boolean hasConsume = false;
        switch (action) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                if (pressTextColor != NO_COLOR) {
                    setTextColor(new Color(pressTextColor));
                }
                if (pressBgColor != NO_COLOR) {
                    mIsPress = true;
                    drawSolid();
                }
                if (drawableClickedListenerEnable(songDrawable, mmiPoint.getX(), mmiPoint.getY()) && !drawableAsBackground) {
                    drawable1Clicked = true;
                }
                if (drawableClickedListenerEnable(drawable2, mmiPoint.getX(), mmiPoint.getY())) {
                    drawable2Clicked = true;
                }
                if (drawable1Clicked || drawable2Clicked) {
                    hasConsume = true;
                }
                break;
            case TouchEvent.PRIMARY_POINT_UP:
            case TouchEvent.CANCEL:
                if (mNormalColor != NO_COLOR) {
                    setTextColor(new Color(mNormalColor));
                }

                if (pressBgColor != NO_COLOR) {
                    mIsPress = false;
                    drawSolid();
                }

                if (onDrawableClickedListener != null) {
                    if (drawable1Clicked) {
                        onDrawableClickedListener.onDrawable1Clicked(this);
                    }
                    if (drawable2Clicked) {
                        onDrawableClickedListener.onDrawable2Clicked(this);
                    }
                }
                drawable1Clicked = false;
                drawable2Clicked = false;
                break;
        }
        return hasConsume;
    }

    private boolean drawableClickedListenerEnable(Element drawable, float x, float y) {
        boolean r = false;
        if (drawable != null) {
            drawable.getBounds().isInclude((int) x, (int) y);
        }
        return r;
    }

    //    @Override
//    public void onWindowBound() {
//        startAnim();
//    }
//
//    @Override
//    public void onWindowUnbound() {
//        stopAnim();
//    }
    @Override
    public void onComponentBoundToWindow(Component component) {
        if (component.getVisibility() != VISIBLE && component.getVisibility() != INVISIBLE) {
            cacheRunnableState = runnable;
            cacheNeedRunState = needRun;
            stopAnim();
        } else if (cacheRunnableState && cacheNeedRunState) {
            startAnim();
        }
        startAnim();
    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {
        stopAnim();
    }

    /**
     * 设置一个监听器，用于监听 Drawable1 或者 Drawable2 的点击事件。
     * 需要注意，对于 Drawable1，只有在它不作为背景图使用使，才能触发点击事件。
     *
     * @param onDrawableClickedListener
     */
    public void setOnDrawableClickedListener(OnDrawableClickedListener onDrawableClickedListener) {
        this.onDrawableClickedListener = onDrawableClickedListener;
    }

    /**
     * 当 Drawable1 被作为背景图时，设置其缩放模式。
     * <p>
     * 具体请参考 {@link ScaleType}，默认缩放模式为 {@link ScaleType#CENTER}
     *
     * @param scaleType
     * @return
     */
    public SuperTextView setScaleType(ScaleType scaleType) {
        if (backgroundScaleType == scaleType) return this;
        this.backgroundScaleType = scaleType;
        drawableBackgroundShader = null;
        invalidate();
        return this;
    }

    /**
     * 当 Drawable1 被作为背景图时，获取其缩放模式。
     *
     * @return
     */
    public ScaleType getScaleType() {
        return backgroundScaleType;
    }

    /**
     * 设置一个跟踪器，用于追踪 SuperTextView 绘制
     *
     * @param tracker
     * @hide
     */
    public void setTracker(Tracker tracker) {
        this.tracker = tracker;
    }


    /**
     * Adjuster被设计用来在SuperTextView的绘制过程中插入一些操作。
     * 这具有非常重要的意义。你可以用它来实现各种各样的效果。比如插入动画、修改状态。
     * 你可以指定Adjuster的作用层级，通过调用{@link Adjuster#setOpportunity(Opportunity)}，
     * {@link Opportunity}。默认为{@link Opportunity#BEFORE_TEXT}。
     * 在Adjuster中，可以获取到控件的触摸事件，这对于实现一些复杂的交互效果很有帮助。
     */
    public static abstract class Adjuster {
        private static final int TYPE_SYSTEM = 0x001;
        private static final int TYPE_CUSTOM = 0x002;

        private Opportunity opportunity = Opportunity.BEFORE_TEXT;
        private int type = TYPE_CUSTOM;

        /**
         * 当前Adjuster被设置到的SuperTextView
         */
        public SuperTextView host;

        /**
         * 在Canvas上绘制的东西将能够呈现在SuperTextView上。
         * 提示：你需要注意图层的使用。
         *
         * @param v      SuperTextView
         * @param canvas 用于绘制的Canvas。注意对Canvas的变换最好使用图层，否则会影响后续的绘制。
         */
        protected abstract void adjust(SuperTextView v, Canvas canvas);

        /**
         * 在这个方法中，你能够捕获到SuperTextView中发生的触摸事件。
         * 需要注意，如果你在该方法中返回了true来处理SuperTextView的触摸事件的话，你将在
         * SuperTextView的setOnTouchListener中设置的OnTouchListener中同样能够捕获到这些触摸事件，即使你在OnTouchListener中返回了false。
         * 但是，如果你在OnTouchListener中返回了true，这个方法将会失效，因为事件在OnTouchListener中被拦截了。
         *
         * @param v     SuperTextView
         * @param event 控件件接收到的触摸事件。
         * @return 默认返回false。如果想持续的处理控件的触摸事件就返回true。否则，只能接收到{@link TouchEvent}事件。
         */
        public boolean onTouch(SuperTextView v, TouchEvent event) {
            return false;
        }

        /**
         * 当Adjuster被通过 {@link SuperTextView#addAdjuster(Adjuster)} 设置到一个SuperTextView中时，
         * 会被调用。用于建立Adjuster与宿主SuperTextView之间的关系。
         *
         * @param stv 当前被设置到的SuperTextView对象
         * @return
         */
        private void attach(SuperTextView stv) {
            this.host = stv;
            onAttach(this.host);
        }

        /**
         * 当Adjuster被通过 {@link SuperTextView#addAdjuster(Adjuster)} 设置到一个SuperTextView中时， 会被调用。
         * <p>
         * 在这个方法中，开发者可以根据当前所处的SuperTextView环境，进行一些初始化的配置。
         *
         * @param stv 当前被设置到的SuperTextView对象
         */
        public void onAttach(SuperTextView stv) {

        }

        /**
         * 当Adjuster被从一个SuperTextView中移除时会被调用，用于解除Adjuster与宿主SuperTextView之间的关系。
         *
         * @param stv 当前被从那个SuperTextView中移除
         * @return
         */
        private void detach(SuperTextView stv) {
            this.host = null;
            onDetach(stv);
        }

        /**
         * 当Adjuster被从一个SuperTextView中移除时会被调用，用于解除Adjuster与宿主SuperTextView之间的关系。
         * <p>
         * 需要注意，在这个方法中，成员变量 {@link Adjuster#host} 已经被释放，不要直接使用该成员变量，而是使用 参数 stv。
         *
         * @param stv 当前被从那个SuperTextView中移除
         * @return
         */
        public void onDetach(SuperTextView stv) {

        }

        /**
         * 获取当前Adjuster的层级。
         *
         * @return Adjuster的作用层级。
         */
        public Opportunity getOpportunity() {
            return opportunity;
        }

        /**
         * 设置Adjuster的作用层级。在 {@link Opportunity} 中可以查看所有支持的层级。
         *
         * @param opportunity Adjuster的作用层级
         * @return 返回Adjuster本身，方便调用。
         */
        public Adjuster setOpportunity(Opportunity opportunity) {
            this.opportunity = opportunity;
            return this;
        }

        /**
         * @hide
         */
        private Adjuster setType(int type) {
            this.type = type;
            return this;
        }

        private int getType() {
            return type;
        }

        /**
         * Adjuster贴心的设计了控制作用层级的功能。
         * 你可以通过{@link Adjuster#setOpportunity(Opportunity)}来指定Adjuster的绘制层级。
         * 在SuperTextView中，绘制层级被从下到上分为：背景层、Drawable层、文字层3个层级。
         * 通过Opportunity来指定你的Adjuster想要插入到那个层级间。
         */
        public static enum Opportunity {
            /**
             * 背景层和Drawable层之间
             */
            BEFORE_DRAWABLE,
            /**
             * Drawable层和文字层之间
             */
            BEFORE_TEXT,
            /**
             * 最顶层
             */
            AT_LAST
        }
    }

    public static enum DrawableLayer {
        /**
         * 在文字下。默认
         */
        BEFORE_TEXT(0),
        /**
         * 在文字上
         */
        AFTER_TEXT(1);

        public int code;

        DrawableLayer(int code) {
            this.code = code;
        }

        public static DrawableLayer valueOf(int code) {
            for (DrawableLayer mode : DrawableLayer.values()) {
                if (mode.code == code) {
                    return mode;
                }
            }
            return BEFORE_TEXT;
        }
    }

    /**
     * 状态图的显示模式。SuperTextView定义了10中显示模式。它们控制着状态图的相对位置。
     * 默认为居中，即{@link DrawableMode#CENTER}。
     */
    public static enum DrawableMode {
        /**
         * 正左
         */
        LEFT(0),
        /**
         * 正上
         */
        TOP(1),
        /**
         * 正右
         */
        RIGHT(2),
        /**
         * 正下
         */
        BOTTOM(3),
        /**
         * 居中
         */
        CENTER(4),
        /**
         * 充满整个控件
         */
        FILL(5),
        /**
         * 左上
         */
        LEFT_TOP(6),
        /**
         * 右上
         */
        RIGHT_TOP(7),
        /**
         * 左下
         */
        LEFT_BOTTOM(8),
        /**
         * 右下
         */
        RIGHT_BOTTOM(9);

        public int code;

        DrawableMode(int code) {
            this.code = code;
        }

        public static DrawableMode valueOf(int code) {
            for (DrawableMode mode : DrawableMode.values()) {
                if (mode.code == code) {
                    return mode;
                }
            }
            return CENTER;
        }
    }

    /**
     * 当 Drawable1 作为背景图时的剪裁模式
     */
    public enum ScaleType {
        /**
         * 将图片拉伸平铺，充满 SuperTextView
         */
        FIT_XY(0),
        /**
         * 将图片自适应居中。
         */
        FIT_CENTER(1),
        /**
         * 将图片剪裁居中。默认值。
         */
        CENTER(2);

        public int code;

        ScaleType(int code) {
            this.code = code;
        }

        public static ScaleType valueOf(int code) {
            for (ScaleType mode : ScaleType.values()) {
                if (mode.code == code) {
                    return mode;
                }
            }
            return CENTER;
        }
    }

    /**
     * 用于监听 Drawable1 和 Drawable2 被点击的事件
     * 需要注意，对于 Drawable1，只有在它不作为背景图使用使，才能触发点击事件。
     */
    public static interface OnDrawableClickedListener {
        /**
         * 当 Drawable1 被点击时触发。
         * 需要注意，对于 Drawable1，只有在它不作为背景图使用使，才能触发点击事件。
         *
         * @param stv
         */
        void onDrawable1Clicked(SuperTextView stv);

        /**
         * 当 Drawable2 被点击时触发。
         *
         * @param stv
         */
        void onDrawable2Clicked(SuperTextView stv);
    }
}
